﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eHRISService
{
   public class Helper
    {
        public static void RecordToLog(String logStr, Exception ex)
        {
            string LogDestination = ConfigurationManager.AppSettings["LogDestination"];

            StreamWriter log = null;
            string Path = AppDomain.CurrentDomain.BaseDirectory;
            string FileName = Path + LogDestination + "eHRISService_log_" + DateTime.Today.ToString("dd-MM-yyyy") + ".txt";
            string LogMessage = string.Empty;

            try
            {
                System.IO.Directory.CreateDirectory(Path + LogDestination);
                if (!File.Exists(FileName))
                {
                    log = new StreamWriter(FileName);
                }
                else
                {
                    log = File.AppendText(FileName);
                }

                if (ex != null)
                {
                    LogMessage = logStr + " Error: " + ex.Message;

                    if (ex.InnerException != null)
                    {
                        LogMessage = LogMessage + " Inner Exception: " + ex.InnerException.Message;
                    }
                }
                else
                {
                    LogMessage = logStr;
                }

                log.WriteLine(DateTime.Now + " | " + LogMessage);
                log.WriteLine();
            }
            catch (Exception etc)
            {
                Console.WriteLine(etc.Message);
            }
            finally
            {
                log.Close();
            }
        }
    }
}
