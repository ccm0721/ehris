﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eHRISService
{
    public class ConstHelpers
    {
        public enum Created_By { AUTO_eEXHIBIT = 1, AUTO_MISSING = 2 }
        public enum Modified_By { AUTO_eEXHIBIT = 1 }
        public enum Country { SIN = 1, MYY = 2, SG = 3, MY = 4 }
        public enum ServiceName { GenerateMissingAttendance = 1, SyncExhibitionMaster = 2, SynceExhibitAttendance = 3, SyncHRISAttendance = 4, SyncPromoterCommIncentive = 5, SyncCalculatePromoterOT = 6 }
        public enum EmailName { HRIS = 1 }
        public enum SystemName { HRIS = 1, AES = 2 }

    }
}
