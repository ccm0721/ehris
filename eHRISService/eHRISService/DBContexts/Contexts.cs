﻿using eHRISService.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eHRISService.DBContexts
{
    public class Contexts
    {
        public class ePaymentContext : DbContext
        {
            public ePaymentContext(string country)
                : base("name=" + country + "ePaymentEntities")
            {
            }

            public virtual DbSet<Exhibition_Exhibition_Attendance> Exhibition_Exhibition_Attendance { get; set; }
        }

        public class AESMasterContext : DbContext
        {
            public AESMasterContext(string country)
                : base("name=" + country + "AESMasterEntities")
            {
            }

            public virtual DbSet<STAFF_T_Staff> Staff_T_Staff { get; set; }
        }

        public class AESBusinessContext : DbContext
        {
            public AESBusinessContext(string country)
                : base("name=" + country + "AESBusinessEntities")
            {
            }

            public virtual DbSet<Exhibition_T_Exhibition> Exhibition_T_Exhibition { get; set; }
        }

        public class AESBrandContext : DbContext
        {
            public AESBrandContext(string country, string brand)
                : base("name=" + country + "AES" + brand + "Entities")
            {
            }
        }

        public class eExhibitionContext : DbContext 
        {
            public eExhibitionContext(string country)
                : base("name=" + country + "eExhibitionEntities")
            {
            }
        }

        public class HrisContext : DbContext
        {
            public HrisContext()
                : base("name=HRISEntities")
            {
            }

            public virtual DbSet<TMS_RT_Staff_eExhibitAtt> TMS_RT_Staff_eExhibitAtt { get; set; }
            public virtual DbSet<TMS_RT_Staff_Attendance_History> TMS_RT_Staff_Attendance_History { get; set; }
            public virtual DbSet<TMS_T_Exhibition> TMS_T_Exhibition { get; set; }
            public virtual DbSet<SETTINGS_T_Company> SETTINGS_T_Company { get; set; }
            public virtual DbSet<SETTINGS_T_Application_Mapping> SETTINGS_T_Application_Mapping { get; set; }
            public virtual DbSet<Hris_STAFF_T_Staff> STAFF_T_Staff { get; set; }
            public virtual DbSet<STAFF_RT_Staff_Movement> STAFF_RT_Staff_Movement { get; set; }
            public virtual DbSet<SETTINGS_T_Scheduler> SETTINGS_T_Scheduler { get; set; }
            public virtual DbSet<SETTINGS_RT_Scheduler_Config> SETTINGS_RT_Scheduler_Config { get; set; }
            public virtual DbSet<STAFF_RT_Event_CommissonIncentive> STAFF_RT_Event_CommissonIncentive { get; set; }
            public virtual DbSet<AUDIT_T_Notification_Log> AUDIT_T_Notification_Log { get; set; }
            public virtual DbSet<STAFF_RT_Staff_ServicePoint> STAFF_RT_Staff_ServicePoint { get; set; }
            public virtual DbSet<SETTINGS_T_System_Parameter> SETTINGS_T_System_Parameter { get; set; }

            protected override void OnModelCreating(DbModelBuilder modelBuilder)
            {
                modelBuilder.Entity<SETTINGS_T_Company>().HasKey(u => new { u.Region_ID, u.Company_ID });
                modelBuilder.Entity<TMS_T_Exhibition>().HasKey(u => new { u.Company_ID, u.Exhibition_ID });
                modelBuilder.Entity<TMS_T_Exhibition>().Property(u => u.SNO).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            }
        }
    }
}
