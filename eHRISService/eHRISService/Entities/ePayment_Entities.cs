﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eHRISService.Entities
{
    [Table("Exhibition.Attendance")]
    public class Exhibition_Exhibition_Attendance
    {
        [Key]
        public int ID { get; set; }
        public DateTime TimeIn { get; set; }
        public DateTime? TimeOut { get; set; }
        public string BrandID { get; set; }
        public string LocationCode { get; set; }
        public string Location { get; set; }
        public string StaffID { get; set; }
        public string StaffName { get; set; }
        public string ExhibitionID { get; set; }
        public string GeoLocationTimeIn { get; set; }
        public string GeoLocationTimeOut { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdateBy { get; set; }
        public string Remark { get; set; }
        public string TotalHours { get; set; }
        public string IpAddress { get; set; }
        public string Status { get; set; }
        public Guid RGUID { get; set; }
    }
}
