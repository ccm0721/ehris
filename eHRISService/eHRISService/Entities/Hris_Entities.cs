﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eHRISService.Entities
{
    [Table("TMS.RT_Staff_eExhibitAtt")]
    public class TMS_RT_Staff_eExhibitAtt
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int SNo { get; set; }
        public string Region_ID { get; set; }
        public string Payroll_ID { get; set; }
        public string Employee_ID { get; set; }
        public string Exhibition_ID { get; set; }
        public DateTime Exhibit_Date { get; set; }
        public DateTime Exhibit_CheckInTime { get; set; }
        public string Exhibit_CheckInLocation { get; set; }
        public DateTime? Exhibit_CheckOutTime { get; set; }
        public string Exhibit_CheckOutLocation { get; set; }
        public string Exhibit_CheckInDistance { get; set; }
        public string Exhibit_CheckOutDistance { get; set; }
        public string Update_Flag { get; set; }
        public string Status { get; set; }
        public string Created_By { get; set; }
        public DateTime Created_DateTime { get; set; }
        public string Modified_By { get; set; }
        public DateTime? Modified_DateTime { get; set; }
        public Guid RGUID { get; set; }
        public int Ref_ID { get; set; }
    }

    [Table("TMS.RT_Staff_Attendance_History")]
    public class TMS_RT_Staff_Attendance_History
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int SNo { get; set; }
        public string Region_ID { get; set; }
        public string Payroll_ID { get; set; }
        public string Employee_ID { get; set; }
        public DateTime Attendance_Date { get; set; }
        public TimeSpan Attendance_Time { get; set; }
        public string Machine_Name { get; set; }
        public string Status { get; set; }
        public string Created_By { get; set; }
        public DateTime Created_DateTime { get; set; }
        public string Status_Code { get; set; }
        public Guid RGUID { get; set; }
    }

    [Table("SETTINGS.T_Company")]
    public class SETTINGS_T_Company
    {
        [Key]
        public string Region_ID { get; set; }
        [Key]
        public string Company_ID { get; set; }
        public string App_ID { get; set; }
        public int Sequence_Number { get; set; }
        public string Company_Name { get; set; }
        public string Company_Name_Short { get; set; }
        public string Company_Name_CN { get; set; }
        public string Company_Name_Short_CN { get; set; }
        public string Company_Reg_Name { get; set; }
        public string Company_Type { get; set; }
        public string Company_MainLine { get; set; }
        public string Company_Fax { get; set; }
        public string Company_Email { get; set; }
        public string GST_No { get; set; }
        public string Registration_No { get; set; }
        public string Company_Logo { get; set; }
        public string Company_Colour { get; set; }
        public DateTime Effective_From { get; set; }
        public DateTime Effective_To { get; set; }
        public string Status { get; set; }
    }

    [Table("SETTINGS.T_Application_Mapping")]
    public class SETTINGS_T_Application_Mapping
    {
        [Key]
        public int SNO { get; set; }
        public string Region_ID { get; set; }
        public string Company_ID { get; set; }
        public string Field_Type { get; set; }
        public string Mapped_From { get; set; }
        public string Mapped_To { get; set; }
        public string Mapped_Type { get; set; }
        public string System_ID { get; set; }
        public string Action_Type { get; set; }
        public string Status { get; set; }
        public Guid RGUID { get; set; }
    }

    [Table("TMS.T_Exhibition")]
    public class TMS_T_Exhibition
    {
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int SNO { get; set; }
        public string Region_ID { get; set; }
        [Key]
        public string Company_ID { get; set; }
        [Key]
        public string Exhibition_ID { get; set; }
        public string Event_Name { get; set; }
        public DateTime Date_From { get; set; }
        public DateTime Date_To { get; set; }
        public TimeSpan Time_From { get; set; }
        public TimeSpan Time_To { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string Status { get; set; }
        public string Created_By { get; set; }
        public DateTime Created_DateTime { get; set; }
        public string Modified_By { get; set; }
        public DateTime? Modified_DateTime { get; set; }
        public Guid RGUID { get; set; }
    }

    [Table("STAFF.T_Staff")]
    public class Hris_STAFF_T_Staff
    {
        [Key]
        public int Employee_SNO { get; set; }
        public string Employee_ID { get; set; }
        public string Payroll_ID { get; set; }
        public string Status { get; set; }
        public string Staff_Email { get; set; }
    }

    [Table("STAFF.RT_Staff_Movement")]
    public class STAFF_RT_Staff_Movement
    {
        [Key]
        public int SNO { get; set; }
        public string Employee_ID { get; set; }
        public string Payroll_ID { get; set; }
        public DateTime Effective_From { get; set; }
        public DateTime Effective_To { get; set; }
        public string Status { get; set; }
    }

    [Table("TMS.T_WorkHourCode")]
    public class TMS_T_WorkHourCode
    {
        [Key]
        public int ID { get; set; }
        public string Schedule_Code { get; set; }
        public string Region_ID { get; set; }
        public string Company_ID { get; set; }
        public string Day { get; set; }
        public TimeSpan Start_Time { get; set; }
        public TimeSpan End_Time { get; set; }
        public decimal? Working_Hours { get; set; }
        public int? Meal_Break_Mins { get; set; }
        public string OT_Type1 { get; set; }
        public string OT_Type2 { get; set; }
        public string OT_Type3 { get; set; }
        public DateTime Effective_From { get; set; }
        public DateTime Effective_To { get; set; }
        public string Status { get; set; }
        public string Created_By { get; set; }
        public DateTime Created_DateTime { get; set; }
        public string Modified_By { get; set; }
        public DateTime? Modified_DateTime { get; set; }
        public Guid RGUID { get; set; }
    }

    [Table("SETTINGS.T_Scheduler")]
    public class SETTINGS_T_Scheduler
    {
        [Key]
        public int ID { get; set; }
        public string Scheduler_Type { get; set; }
        public string Scheduler_Name { get; set; }
        public string Status { get; set; }
        public Guid RGUID { get; set; }
    }

    [Table("SETTINGS.RT_Scheduler_Config")]
    public class SETTINGS_RT_Scheduler_Config
    {
        public int Scheduler_ID { get; set; }
        public string Region_ID { get; set; }
        public string Company_ID { get; set; }
        public string Branch_ID { get; set; }
        public string Company_Type { get; set; }
        public string Filter_Type { get; set; }
        public int? Filter_From { get; set; }
        public int? Filter_To { get; set; }
        public DateTime? Filter_Date_From { get; set; }
        public DateTime? Filter_Date_To { get; set; }
        public string Filter_Employee_ID { get; set; }
        public string Filter_Payroll_ID { get; set; }
        public string Email_Server { get; set; }
        public int? Email_Port { get; set; }
        public bool? Email_SSL { get; set; }
        public string Email_Login { get; set; }
        public string Email_Password { get; set; }
        public string Email_From { get; set; }
        public string Email_To { get; set; }
        public string Email_CC { get; set; }
        public string Email_BCC { get; set; }
        public string Email_Title { get; set; }
        public string Email_Message { get; set; }
        public string Email_Error_Title { get; set; }
        public string Email_Error_Message { get; set; }
        public string Email_Exception { get; set; }
        public string Mobile_To { get; set; }
        public string Mobile_Message { get; set; }
        public string Mobile_Error_Message { get; set; }
        public string Mobile_Exception { get; set; }
        public string Notification_To { get; set; }
        public string Notification_Link { get; set; }
        public string Notification_Hub { get; set; }
        public string Notification_Title { get; set; }
        public string Notification_Message { get; set; }
        public string Notification_Error_Message { get; set; }
        public string Notification_Exception { get; set; }
        public bool Status { get; set; }
        [Key]
        public Guid RGUID { get; set; }
    }

    [Table("STAFF.RT_Event_CommissionIncentive")]
    public class STAFF_RT_Event_CommissonIncentive
    {
        [Key]
        public int SNO { get; set; }
        public string Region_ID { get; set; }
        public string Employee_ID { get; set; }
        public string Event_Company_ID { get; set; }
        public decimal Comm_Amount { get; set; }
        public decimal Incentive_Amount { get; set; }
        public DateTime Comm_Period { get; set; }
        public string Created_By { get; set; }
        public DateTime Created_DateTime { get; set; }
        public bool Status { get; set; }
        public Guid RGUID { get; set; }
    }

    [Table("AUDIT.T_Notification_Log")]
    public class AUDIT_T_Notification_Log
    {
        [Key]
        public int SNO { get; set; }
        public string Region_ID { get; set; }
        public string Employee_ID { get; set; }
        public string Payroll_ID { get; set; }
        public string Notification_Message { get; set; }
        public string Notification_Name { get; set; }
        public string Created_By { get; set; }
        public DateTime Created_DateTime { get; set; }
        public string Status { get; set; }
    }

    [Table("Staff.RT_Staff_ServicePoint")]
    public class STAFF_RT_Staff_ServicePoint
    {
        [Key]
        public int SNO { get; set; }
        public string Region_ID { get; set; }
        public string Company_ID { get; set; }
        public string Employee_ID { get; set; }
        public string Staff_ID { get; set; }
        public System.DateTime Effective_From { get; set; }
        public System.DateTime Effective_To { get; set; }
        public Nullable<decimal> Service_Point { get; set; }
        public string Status { get; set; }
        public string Created_By { get; set; }
        public System.DateTime Created_DateTime { get; set; }
        public string Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_DateTime { get; set; }
    }

    [Table("Settings.T_System_Parameter")]
    public class SETTINGS_T_System_Parameter
    {
        [Key]
        public int SNO { get; set; }
        public string Region_ID { get; set; }
        public string Parameter_Name { get; set; }
        public string Parameter_Type { get; set; }
        public string Parameter_Value { get; set; }
        public string Remarks { get; set; }
        public DateTime Effective_From { get; set; }
        public DateTime Effective_To { get; set; }    
        public string Status { get; set; }
        public string Created_By { get; set; }
        public System.DateTime Created_DateTime { get; set; }
        public string Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_DateTime { get; set; }
        public Guid RGUID { get; set; }
    }




}
