﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eHRISService.Entities
{
    public partial class EmailEntity
    {
        public string Subject { get; set; }
        public string ErrorSubject { get; set; }
        public string Body { get; set; }
        public string ErrorBody { get; set; }
        public string FromEmail { get; set; }
        public string ToEmail { get; set; }  
        public string ErrorToEmail { get; set; }
        public string CcEmail { get; set; }
        public string BccEmail { get; set; }
        public string SmtpServer { get; set; }
        public int? SmtpPort { get; set; }
        public int SmtpSSL { get; set; }
        public string SmtpLogin { get; set; }
        public string SmtpPassword { get; set; }
    }

    public partial class ServiceEntity
    {
        public int SchedulerId { get; set; }
        public string Country { get; set; }
        public string Brand { get; set; }
        public string CompanyID { get; set; }
        public string BranchID { get; set; }
        public string EmployeeID { get; set; }
        public string PayrollID { get; set; }
        public int? DaysFrom { get; set; }
        public int? DaysTo { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public string Notification_Exception { get; set; }
        public string CompanyType { get; set; }
        public string CheckType { get; set; }
    }

    public class Exhibition_csp_ReadHrisCommIncentive
    {
        public string Company_ID { get; set; }
        public string Promoter_ID { get; set; }
        public string HRIS_Employee_ID { get; set; }
        public DateTime Comm_Date { get; set; }
        public decimal Comm_Amount { get; set; }
        public decimal Incentive_Amount { get; set; }
    }

    public class TMS_csp_GetStaffAttendanceNotification
    {
        public string Region_ID { get; set; }
        public string Company_ID { get; set; }
        public string Branch_ID { get; set; }
        public string Company_Type { get; set; }
        public string Employee_ID { get; set; }
        public string Payroll_ID { get; set; }
        public string Staff_Name { get; set; }
        public string Staff_Alias { get; set; }
        public string Staff_Job_Level { get; set; }
        public string Staff_Designation { get; set; }
        public string Staff_HPNo { get; set; }
        public DateTime Date_Range { get; set; }
        public string ShiftCode { get; set; }
        public TimeSpan? ShiftTimeIn { get; set; }
        public TimeSpan? ShiftTimeOut { get; set; }
        public TimeSpan? StartWork_Time { get; set; }
        public TimeSpan? EndWork_Time { get; set; }
    }

    public class AzureNotificationEntity
    {
        public string title { get; set; }
        public string message { get; set; }
        public string to_tag { get; set; }
        public string startPage { get; set; }
    }

    public class AES_Staff_csp_ReadHRTrainingTransactions
    {
        public string HRIS_Employee_ID { get; set; }
        public string Staff_ID { get; set; }
        public Nullable<int> trans_Month { get; set; }
        public Nullable<int> Trans_Year { get; set; }
        public string Staff_Name { get; set; }
        public string Staff_Alias { get; set; }
        public string Outlet { get; set; }
        public string Job_Level { get; set; }
        public Nullable<System.DateTime> Join_Date { get; set; }
        public Nullable<System.DateTime> Resign_Date { get; set; }
        public decimal? ServicePoint { get; set; }
        public string Date { get; set; }
    }
}
