﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eHRISService.Entities
{
    [Table("STAFF.T_Staff")]
    public class STAFF_T_Staff
    {
        [Key]
        public Guid SNo { get; set; }
        public string Staff_ID { get; set; }
        public int Staff_No { get; set; }
        public string Staff_Title { get; set; }
        public string Staff_Name { get; set; }
        public string Staff_Alias { get; set; }
        public string Staff_NRIC { get; set; }
        public string Staff_Gender { get; set; }
        public string Staff_Race { get; set; }
        public DateTime? Staff_DOB { get; set; }
        public string Staff_Marital_Status { get; set; }
        public string Staff_TelNo { get; set; }
        public string Staff_HPNo { get; set; }
        public string Staff_Email { get; set; }
        public string Staff_Address { get; set; }
        public string Staff_Status { get; set; }
        public string HRIS_Employee_ID { get; set; }
        public string Employee_ID { get; set; }
        public string Company_ID { get; set; }
        public DateTime? Resign_Date { get; set; }
    }
}
