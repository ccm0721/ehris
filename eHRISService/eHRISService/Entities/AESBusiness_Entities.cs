﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eHRISService.Entities
{
    [Table("Exhibition.T_Exhibition")]
    public class Exhibition_T_Exhibition
    {
        public string Company_ID { get; set; }
        public int Exhibition_No { get; set; }
        public string Location { get; set; }
        public string Location_Code { get; set; }
        [Key]
        public string Exhibition_ID { get; set; }
        public string Event_Name { get; set; }
        public DateTime Date_From { get; set; }
        public DateTime Date_To { get; set; }
        public TimeSpan Time_From { get; set; }
        public TimeSpan Time_To { get; set; }
        public string Event_Status { get; set; }
        public string Status { get; set; }
        public DateTime Created_DateTime { get; set; }
        public DateTime? Modified_DateTime { get; set; }
        public string Location_Latitude { get; set; }
        public string Location_Longitude { get; set; }
    }
}
