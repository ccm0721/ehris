﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.IO;
using System.Xml;
using System.Collections;
using System.Configuration;
using eHRISService.Entities;
using eHRISService.Controllers.eExhibit;
using eHRISService.Controllers.Hris;
using System.Net.Http;
using System.Threading;
using System.Net.Http.Headers;
using System.Data.Entity;
using eHRISService.Controllers.AES;

namespace eHRISService
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                AutoService autoService = new AutoService();

                string[] splitParameter;

                splitParameter = args[0].Split(';');

                //Parameters: SchedulerId;Country,Testing,Testing2,Testing3

                //Read Scheduler Config
                autoService.ReadSchedulerConfig(splitParameter[0], splitParameter[1]);

                autoService.GenerateService();

                Environment.Exit(0);
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("Main", ex);

                Environment.Exit(1);
            }
        }
    }

    public class AutoService
    {
        static string OAuthApi = ConfigurationManager.AppSettings["OAuthApi"];
        static string eHRISApi = ConfigurationManager.AppSettings["eHRISApi"];
        const string reportConfig = @"\\config.xml";
        private EmailEntity emailEntity = new EmailEntity();
        private ServiceEntity serviceEntity = new ServiceEntity();
        private string startUpPath = AppDomain.CurrentDomain.BaseDirectory;
        private DBContexts.Contexts.HrisContext dbHris = new DBContexts.Contexts.HrisContext();
        private DBContexts.Contexts.ePaymentContext dbePayment = new DBContexts.Contexts.ePaymentContext("SG");
        private DBContexts.Contexts.AESMasterContext dbAESMaster = new DBContexts.Contexts.AESMasterContext("SG");
        private DBContexts.Contexts.AESBrandContext dbAESBrand = new DBContexts.Contexts.AESBrandContext("SG", "LWM");
        private List<SETTINGS_RT_Scheduler_Config> lstSchedulerConfig = new List<SETTINGS_RT_Scheduler_Config>();
        private string attCheckType = "ATT";

        private string mappedCountry = string.Empty;
        private string mappedCompany = string.Empty;

        public void ReadSchedulerConfig(string command, string country)
        {
            try
            {
                lstSchedulerConfig = new Hris_Settings().GetSchedulerConfigById(Convert.ToInt32(command), ref dbHris);

                lstSchedulerConfig = lstSchedulerConfig.Where(x => x.Region_ID == country).ToList();
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("ReadSchedulerConfig", ex);

                throw new ArgumentException("ReadSchedulerConfig Error: " + ex.Message);
            }
        }

        public void GenerateService()
        {
            try
            {
                Helper.RecordToLog("Start Generate Service.", null);

                foreach(var objScheduler in lstSchedulerConfig)
                {
                    GenerateFilterParams(objScheduler);
                    GenerateEmailParams(objScheduler);

                    switch (serviceEntity.SchedulerId)
                    {
                        case 1: //TMS Missing Attendance
                            GenerateMissingAttendanceNotification(objScheduler);
                            break;
                        case 2: //Exhibition Master
                            SyncExhibitionMaster();
                            break;
                        case 3: //Exhibition Attendance
                            SynceExhibitAttendance();
                            break;
                        case 4: //TMS Attendance
                            SyncHRISAttendance();
                            break;
                        case 5:  //Promoter TMS Attendance
                            serviceEntity.CheckType = "MOD";
                            SyncHRISAttendance();
                            break;
                        case 6:
                            SyncPromoterCommIncentive();
                            break;
                        case 7:
                            SyncCalculatePromoterOT();
                            break;
                        case 8:
                            SyncServicePoints();
                            break;
                    }
                }

                if (serviceEntity.SchedulerId == 3)
                {
                    ReadSchedulerConfig("5", serviceEntity.Country);

                    Helper.RecordToLog("Total:" + lstSchedulerConfig.Count().ToString(), null);

                    GenerateService();
                }

                Helper.RecordToLog("End Generate Service.", null);
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("GenerateService", ex);

                throw new ArgumentException("GenerateService Error: " + ex.Message);
            }
        }

        public void GenerateFilterParams(SETTINGS_RT_Scheduler_Config objSchedulerConfig)
        {
            try
            {
                serviceEntity = new ServiceEntity();

                serviceEntity.SchedulerId = objSchedulerConfig.Scheduler_ID;
                serviceEntity.Country = String.IsNullOrEmpty(objSchedulerConfig.Region_ID) ? null : objSchedulerConfig.Region_ID;
                serviceEntity.Brand = "";
                serviceEntity.CompanyID = (String.IsNullOrEmpty(objSchedulerConfig.Company_ID) || objSchedulerConfig.Company_ID == "ALL") ? null : objSchedulerConfig.Company_ID;
                serviceEntity.BranchID = (String.IsNullOrEmpty(objSchedulerConfig.Branch_ID) || objSchedulerConfig.Branch_ID == "ALL") ? null : objSchedulerConfig.Branch_ID;
                serviceEntity.EmployeeID = (String.IsNullOrEmpty(objSchedulerConfig.Filter_Employee_ID) || objSchedulerConfig.Filter_Employee_ID == "ALL") ? null : objSchedulerConfig.Filter_Employee_ID;
                serviceEntity.PayrollID = (String.IsNullOrEmpty(objSchedulerConfig.Filter_Payroll_ID) || objSchedulerConfig.Filter_Payroll_ID == "ALL") ? null : objSchedulerConfig.Filter_Payroll_ID;
                serviceEntity.DaysFrom = objSchedulerConfig.Filter_From == null ? 0 : objSchedulerConfig.Filter_From;
                serviceEntity.DaysTo = objSchedulerConfig.Filter_To == null ? 0 : objSchedulerConfig.Filter_To;
                serviceEntity.Notification_Exception = String.IsNullOrEmpty(objSchedulerConfig.Notification_Exception) ? null : objSchedulerConfig.Notification_Exception;
                serviceEntity.CompanyType = (String.IsNullOrEmpty(objSchedulerConfig.Company_Type) || objSchedulerConfig.Company_Type == "ALL") ? null : objSchedulerConfig.Company_Type;
                serviceEntity.CheckType = attCheckType;

                DateTime startDate = DateTime.Now.Date;
                DateTime endDate = DateTime.Now.Date;

                switch (objSchedulerConfig.Filter_Type.ToUpper())
                {
                    case "DATE":
                        serviceEntity.DateFrom = objSchedulerConfig.Filter_Date_From == null ? DateTime.Now.Date : objSchedulerConfig.Filter_Date_From;
                        serviceEntity.DateTo = objSchedulerConfig.Filter_Date_To == null ? DateTime.Now.Date : objSchedulerConfig.Filter_Date_To;
                        break;
                    case "MONTHLY":
                        startDate = DateTime.Now.AddMonths(objSchedulerConfig.Filter_From == null ? 0 : (int)objSchedulerConfig.Filter_From);
                        startDate = new DateTime(startDate.Year, startDate.Month, 1);

                        endDate = DateTime.Now.AddMonths(objSchedulerConfig.Filter_To == null ? 0 : (int)objSchedulerConfig.Filter_To);
                        endDate = new DateTime(endDate.Year, endDate.Month, DateTime.DaysInMonth(endDate.Year, endDate.Month));

                        serviceEntity.DateFrom = startDate;
                        serviceEntity.DateTo = endDate;
                        break;
                    case "DAILY":
                        serviceEntity.DateFrom = DateTime.Now.AddDays(objSchedulerConfig.Filter_From == null ? 0 : (int)objSchedulerConfig.Filter_From);
                        serviceEntity.DateTo = DateTime.Now.AddDays(objSchedulerConfig.Filter_To == null ? 0 : (int)objSchedulerConfig.Filter_To);
                        break;
                    case "YEARLY":
                        startDate = DateTime.Now.AddYears(objSchedulerConfig.Filter_From == null ? 0 : (int)objSchedulerConfig.Filter_From);
                        startDate = new DateTime(startDate.Year, startDate.Month, 1);

                        endDate = DateTime.Now.AddYears(objSchedulerConfig.Filter_To == null ? 0 : (int)objSchedulerConfig.Filter_To);
                        endDate = new DateTime(endDate.Year, endDate.Month, DateTime.DaysInMonth(endDate.Year, endDate.Month));

                        serviceEntity.DateFrom = startDate;
                        serviceEntity.DateTo = endDate;
                        break;
                }
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("GenerateFilterParams", ex);

                throw new ArgumentException("GenerateFilterParams Error: " + ex.Message);
            }
        }

        public void GenerateEmailParams(SETTINGS_RT_Scheduler_Config objSchedulerConfig)
        {
            try
            {
                emailEntity = new EmailEntity();

                emailEntity.Subject = objSchedulerConfig.Email_Title;
                emailEntity.Body = objSchedulerConfig.Email_Message;
                emailEntity.FromEmail = objSchedulerConfig.Email_From;
                emailEntity.ToEmail = objSchedulerConfig.Email_To;
                emailEntity.CcEmail = objSchedulerConfig.Email_CC;
                emailEntity.BccEmail = objSchedulerConfig.Email_BCC;
                emailEntity.SmtpServer = objSchedulerConfig.Email_Server;
                emailEntity.SmtpPort = objSchedulerConfig.Email_Port;
                emailEntity.SmtpSSL = Convert.ToInt32(objSchedulerConfig.Email_SSL);
                emailEntity.SmtpLogin = objSchedulerConfig.Email_Login;
                emailEntity.SmtpPassword = objSchedulerConfig.Email_Password;

                emailEntity.ErrorSubject = objSchedulerConfig.Email_Error_Title;
                emailEntity.ErrorBody = objSchedulerConfig.Email_Error_Message;
                emailEntity.ErrorToEmail = objSchedulerConfig.Email_Exception;
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("GenerateEmailParams", ex);

                throw new ArgumentException("GenerateEmailParams Error: " + ex.Message);
            }
        }

        public void GenerateMissingAttendanceNotification(SETTINGS_RT_Scheduler_Config objScheduler)
        {
            try
            {
                Helper.RecordToLog("Start " + ConstHelpers.ServiceName.GenerateMissingAttendance.ToString(), null);

                var notificationList = new Hris_Tms().GetStaffAttendanceNotification(serviceEntity, ref dbHris);

                foreach (var notification in notificationList)
                {
                    if (notification.ShiftTimeIn != null || notification.StartWork_Time != null)
                    {
                        if (DateTime.Now.TimeOfDay > (notification.ShiftTimeIn == null ? notification.StartWork_Time : notification.ShiftTimeIn))
                        {
                            //Post Notification
                            string[] splitNotificationTo = !String.IsNullOrEmpty(objScheduler.Notification_To) ? objScheduler.Notification_To.Split(',') : notification.Payroll_ID.Split(',');

                            foreach (var notificationTo in splitNotificationTo)
                            {
                                AzureNotificationEntity azureNotify = new AzureNotificationEntity();

                                azureNotify.title = objScheduler.Notification_Title;
                                azureNotify.message = string.Format(objScheduler.Notification_Message, notification.Staff_Name);
                                azureNotify.to_tag = "uid:" + notificationTo.ToString();
                                azureNotify.startPage = objScheduler.Notification_Link;

                                HttpResponseMessage response = new Others().PostAzureNotification(azureNotify, objScheduler.Notification_Hub, OAuthApi);

                                AUDIT_T_Notification_Log notificationLog = new AUDIT_T_Notification_Log();
                                notificationLog.Region_ID = notification.Region_ID;
                                notificationLog.Employee_ID = notification.Employee_ID;
                                notificationLog.Payroll_ID = notification.Payroll_ID;
                                notificationLog.Notification_Message = azureNotify.message;
                                notificationLog.Notification_Name = ConstHelpers.ServiceName.GenerateMissingAttendance.ToString();
                                notificationLog.Status = Convert.ToInt32(response.IsSuccessStatusCode).ToString();
                                notificationLog.Created_By = ConstHelpers.Created_By.AUTO_MISSING.ToString();
                                notificationLog.Created_DateTime = DateTime.Now;

                                new Hris_Audit().PostNotificationLog(notificationLog, ref dbHris);
                            }
                        }
                    }
                }

                Helper.RecordToLog("End " + ConstHelpers.ServiceName.GenerateMissingAttendance.ToString(), null);
            }
            catch (Exception ex)
            {
                GenerateErrorEmail("[" + ConstHelpers.EmailName.HRIS.ToString() + "] " + ConstHelpers.ServiceName.GenerateMissingAttendance.ToString(), serviceEntity.Country, serviceEntity.CompanyID, serviceEntity.BranchID, ConstHelpers.EmailName.HRIS.ToString(), ConstHelpers.ServiceName.GenerateMissingAttendance.ToString(), ex);

                Helper.RecordToLog(ConstHelpers.ServiceName.GenerateMissingAttendance.ToString(), ex);

                throw new ArgumentException(ConstHelpers.ServiceName.GenerateMissingAttendance.ToString() + " Error: " + ex.Message);
            }
        }

        public void SyncExhibitionMaster()
        {
            try
            {
                Helper.RecordToLog("Start " + ConstHelpers.ServiceName.SyncExhibitionMaster.ToString(), null);

                List<Exhibition_T_Exhibition> lstExhibition = new List<Exhibition_T_Exhibition>();
                List<SETTINGS_T_Application_Mapping> lstApplicationMapping = new List<SETTINGS_T_Application_Mapping>();
                List<TMS_T_Exhibition> lstTMSExhibition = new List<TMS_T_Exhibition>();

                lstApplicationMapping = new Hris_Settings().GetApplicationMapping();

                ReMapServiceEntity(serviceEntity.Country, serviceEntity.CompanyID, ConstHelpers.SystemName.AES.ToString(), true);

                lstExhibition = new eExhibit().GetExhibitionMaster(serviceEntity);

                using (var _dbTran = dbHris.Database.BeginTransaction())
                {
                    foreach (var exhibition in lstExhibition)
                    {
                        TMS_T_Exhibition objExhibition = new TMS_T_Exhibition();

                        string mappedTo = lstApplicationMapping.Where(x => x.Mapped_From == exhibition.Company_ID && x.Field_Type == "COMPANY_ID" && x.System_ID == "EXHIBITION" && x.Action_Type == "PULL" && x.Status == "0").Select(x => x.Mapped_To).FirstOrDefault();

                        //Check Existent
                        var tmsExhibition = dbHris.TMS_T_Exhibition.Where(x => x.Company_ID == mappedTo && x.Exhibition_ID == exhibition.Exhibition_ID && x.Status == "0").FirstOrDefault();

                        if (tmsExhibition != null)
                        {
                            if (tmsExhibition.Event_Name != exhibition.Event_Name || tmsExhibition.Date_From != exhibition.Date_From || tmsExhibition.Date_To != exhibition.Date_To
                             || tmsExhibition.Time_From != exhibition.Time_From || tmsExhibition.Time_To != exhibition.Time_To || (tmsExhibition.Status == "0" && (exhibition.Event_Status == "V" || exhibition.Status == "1")))
                            {
                                tmsExhibition.Event_Name = exhibition.Event_Name;
                                tmsExhibition.Date_From = exhibition.Date_From;
                                tmsExhibition.Date_To = exhibition.Date_To;
                                tmsExhibition.Time_From = exhibition.Time_From;
                                tmsExhibition.Time_To = exhibition.Time_To;
                                tmsExhibition.Longitude = exhibition.Location_Longitude;
                                tmsExhibition.Latitude = exhibition.Location_Latitude;
                                tmsExhibition.Status = (exhibition.Event_Status == "V" || exhibition.Status == "1") ? "1" : "0";
                                tmsExhibition.Modified_By = ConstHelpers.Modified_By.AUTO_eEXHIBIT.ToString();
                                tmsExhibition.Modified_DateTime = DateTime.Now;

                                new Hris_Tms().UpdateExhibition(tmsExhibition, ref dbHris);
                            }
                        }
                        else
                        {
                            objExhibition.Region_ID = exhibition.Company_ID.Contains("SG") ? ConstHelpers.Country.SIN.ToString() : ConstHelpers.Country.MYY.ToString();
                            objExhibition.Company_ID = mappedTo;
                            objExhibition.Exhibition_ID = exhibition.Exhibition_ID;
                            objExhibition.Event_Name = exhibition.Event_Name;
                            objExhibition.Date_From = exhibition.Date_From;
                            objExhibition.Date_To = exhibition.Date_To;
                            objExhibition.Time_From = exhibition.Time_From;
                            objExhibition.Time_To = exhibition.Time_To;
                            objExhibition.Longitude = exhibition.Location_Longitude;
                            objExhibition.Latitude = exhibition.Location_Latitude;
                            objExhibition.Status = "0";
                            objExhibition.Created_By = ConstHelpers.Created_By.AUTO_eEXHIBIT.ToString();
                            objExhibition.Created_DateTime = DateTime.Now;
                            objExhibition.RGUID = Guid.NewGuid();

                            lstTMSExhibition.Add(objExhibition);
                        }
                    }

                    if (lstTMSExhibition.Count > 0) { new Hris_Tms().InsertExhibition(lstTMSExhibition, ref dbHris); }

                    dbHris.SaveChanges();
                    _dbTran.Commit();
                }

                Helper.RecordToLog("End " + ConstHelpers.ServiceName.SyncExhibitionMaster.ToString(), null);
            }
            catch (Exception ex)
            {
                GenerateErrorEmail("[" + ConstHelpers.EmailName.HRIS.ToString() + "] " + ConstHelpers.ServiceName.SyncExhibitionMaster.ToString(), serviceEntity.Country, serviceEntity.CompanyID, serviceEntity.BranchID, ConstHelpers.EmailName.HRIS.ToString(), ConstHelpers.ServiceName.SyncExhibitionMaster.ToString(), ex);

                Helper.RecordToLog(ConstHelpers.ServiceName.SyncExhibitionMaster.ToString(), ex);

                throw new ArgumentException(ConstHelpers.ServiceName.SyncExhibitionMaster.ToString() + " Error: " + ex.Message);
            }
        }

        public void SynceExhibitAttendance()
        {
            try
            {
                Helper.RecordToLog("Start " + ConstHelpers.ServiceName.SynceExhibitAttendance.ToString(), null);

                List<Exhibition_Exhibition_Attendance> lstExhibitAtt = new List<Exhibition_Exhibition_Attendance>();
                List<TMS_RT_Staff_eExhibitAtt> lstTMSExhibitAtt = new List<TMS_RT_Staff_eExhibitAtt>();

                //SETTINGS_T_System_Parameter objSystemParameter = new Hris_Settings().GetSystemParameterByName(serviceEntity.Country, "PROMOTER_DISTANCE_CAP", ref dbHris);

                ReMapServiceEntity(serviceEntity.Country, serviceEntity.CompanyID, ConstHelpers.SystemName.AES.ToString(), true);

                dbAESMaster = new DBContexts.Contexts.AESMasterContext(serviceEntity.Country);

                //Pull Data From eExhibit Attendance
                lstExhibitAtt = new eExhibit().GetExhibitionAttendance(serviceEntity);
                //lstExhibitAtt = lstExhibitAtt.Where(x => x.ExhibitionID == "LWEX17-81").ToList();

                using (var _dbTran = dbHris.Database.BeginTransaction())
                {
                    foreach (var attendance in lstExhibitAtt)
                    {
                        TMS_RT_Staff_eExhibitAtt objAttendance = new TMS_RT_Staff_eExhibitAtt();

                        ReMapServiceEntity(serviceEntity.Country, serviceEntity.CompanyID, ConstHelpers.SystemName.HRIS.ToString(), true);

                        //Get Employee ID from AES
                        var staff = dbAESMaster.Staff_T_Staff.Where(x => x.Staff_ID == attendance.StaffID && x.Staff_Status == "A").FirstOrDefault();
                        string employeeID = string.Empty;
                        string payrollID = string.Empty;

                        if (staff != null)
                        {
                            //Get Staff Details from HRIS
                            var hrisStaff = dbHris.STAFF_RT_Staff_Movement.Where(x => x.Employee_ID == staff.HRIS_Employee_ID && attendance.TimeIn.Date >= x.Effective_From && attendance.TimeIn.Date <= x.Effective_To && x.Status == "0").FirstOrDefault();

                            if (hrisStaff != null)
                            {
                                employeeID = hrisStaff.Employee_ID;
                                payrollID = hrisStaff.Payroll_ID;
                            }
                        }

                        //Check Existent
                        var objExhibitionAttendance = dbHris.TMS_RT_Staff_eExhibitAtt.Where(x => x.Ref_ID == attendance.ID && x.Region_ID == serviceEntity.Country && x.Status == "0").FirstOrDefault();
                        var objExhibition = dbHris.TMS_T_Exhibition.Where(x => x.Region_ID == serviceEntity.Country && x.Exhibition_ID == attendance.ExhibitionID && x.Status == "0").FirstOrDefault();

                        if (objExhibitionAttendance != null)
                        {
                            if (objExhibitionAttendance.Exhibit_CheckInTime != attendance.TimeIn || String.IsNullOrEmpty(objExhibitionAttendance.Exhibit_CheckInLocation) != String.IsNullOrEmpty(attendance.GeoLocationTimeIn) || objExhibitionAttendance.Exhibit_CheckOutTime != attendance.TimeOut
                             || objExhibitionAttendance.Exhibit_CheckOutLocation != attendance.GeoLocationTimeOut || (objExhibitionAttendance.Status == "0" && attendance.Status == "V"))
                            {
                                objExhibitionAttendance.Region_ID = serviceEntity.Country;
                                objExhibitionAttendance.Payroll_ID = payrollID;
                                objExhibitionAttendance.Employee_ID = employeeID;
                                objExhibitionAttendance.Exhibition_ID = attendance.ExhibitionID;
                                objExhibitionAttendance.Exhibit_Date = attendance.TimeIn.Date;
                                objExhibitionAttendance.Exhibit_CheckInTime = attendance.TimeIn;
                                objExhibitionAttendance.Exhibit_CheckInLocation = attendance.GeoLocationTimeIn;
                                objExhibitionAttendance.Exhibit_CheckOutTime = attendance.TimeOut;
                                objExhibitionAttendance.Exhibit_CheckOutLocation = attendance.GeoLocationTimeOut;

                                if (objExhibition != null)
                                {
                                    if (String.IsNullOrEmpty(objExhibition.Latitude) || String.IsNullOrEmpty(objExhibition.Longitude) || String.IsNullOrEmpty(attendance.GeoLocationTimeIn))
                                    {
                                        objExhibitionAttendance.Exhibit_CheckInDistance = "NA";
                                    }
                                    else
                                    {
                                        objExhibitionAttendance.Exhibit_CheckInDistance = Convert.ToString(GetDistanceByCoordinates(objExhibition.Latitude.ToString() + ";" + objExhibition.Longitude.ToString(), attendance.GeoLocationTimeIn));
                                    }

                                    if (String.IsNullOrEmpty(objExhibition.Latitude) || String.IsNullOrEmpty(objExhibition.Longitude) || String.IsNullOrEmpty(attendance.GeoLocationTimeOut))
                                    {
                                        objExhibitionAttendance.Exhibit_CheckOutDistance = "NA";
                                    }
                                    else
                                    {
                                        objExhibitionAttendance.Exhibit_CheckOutDistance = Convert.ToString(GetDistanceByCoordinates(objExhibition.Latitude.ToString() + ";" + objExhibition.Longitude.ToString(), attendance.GeoLocationTimeOut));
                                    }
                                }
                                else
                                {
                                    objExhibitionAttendance.Exhibit_CheckInDistance = "NA";
                                    objExhibitionAttendance.Exhibit_CheckOutDistance = "NA";
                                }
                             
                                objExhibitionAttendance.Update_Flag = "0";
                                objExhibitionAttendance.Status = attendance.Status == "V" ? "1" : "0";
                                objExhibitionAttendance.Modified_By = ConstHelpers.Modified_By.AUTO_eEXHIBIT.ToString();
                                objExhibitionAttendance.Modified_DateTime = DateTime.Now;

                                new Hris_Tms().UpdateExhibitionAtt(objExhibitionAttendance, ref dbHris);
                            }
                        }
                        else
                        {
                            objAttendance.Region_ID = serviceEntity.Country;
                            objAttendance.Payroll_ID = payrollID;
                            objAttendance.Employee_ID = employeeID;
                            objAttendance.Exhibition_ID = attendance.ExhibitionID;
                            objAttendance.Exhibit_Date = attendance.TimeIn.Date;
                            objAttendance.Exhibit_CheckInTime = attendance.TimeIn;
                            objAttendance.Exhibit_CheckInLocation = attendance.GeoLocationTimeIn;
                            objAttendance.Exhibit_CheckOutTime = attendance.TimeOut;
                            objAttendance.Exhibit_CheckOutLocation = attendance.GeoLocationTimeOut;

                            if (objExhibition != null)
                            {
                                if (String.IsNullOrEmpty(objExhibition.Latitude) || String.IsNullOrEmpty(objExhibition.Longitude) || String.IsNullOrEmpty(attendance.GeoLocationTimeIn))
                                {
                                    objAttendance.Exhibit_CheckInDistance = "NA";
                                }
                                else
                                {
                                    objAttendance.Exhibit_CheckInDistance = Convert.ToString(GetDistanceByCoordinates(objExhibition.Latitude.ToString() + ";" + objExhibition.Longitude.ToString(), attendance.GeoLocationTimeIn));
                                }

                                if (String.IsNullOrEmpty(objExhibition.Latitude) || String.IsNullOrEmpty(objExhibition.Longitude) || String.IsNullOrEmpty(attendance.GeoLocationTimeOut))
                                {
                                    objAttendance.Exhibit_CheckOutDistance = "NA";
                                }
                                else
                                {
                                    objAttendance.Exhibit_CheckOutDistance = Convert.ToString(GetDistanceByCoordinates(objExhibition.Latitude.ToString() + ";" + objExhibition.Longitude.ToString(), attendance.GeoLocationTimeOut));
                                }
                            }
                            else
                            {
                                objAttendance.Exhibit_CheckInDistance = "NA";
                                objAttendance.Exhibit_CheckOutDistance = "NA";
                            }

                            objAttendance.Update_Flag = "0";
                            objAttendance.Status = "0";
                            objAttendance.Created_By = ConstHelpers.Created_By.AUTO_eEXHIBIT.ToString();
                            objAttendance.Created_DateTime = DateTime.Now;
                            objAttendance.RGUID = Guid.NewGuid();
                            objAttendance.Ref_ID = attendance.ID;

                            lstTMSExhibitAtt.Add(objAttendance);
                        }
                    }

                    if (lstTMSExhibitAtt.Count > 0) { new Hris_Tms().InsertExhibitionAtt(lstTMSExhibitAtt, ref dbHris); }

                    dbHris.SaveChanges();
                    _dbTran.Commit();
                }

                //Post to Attendance History
                using ( var _dbTran = dbHris.Database.BeginTransaction())
                {
                    //Get eExhibit Attendance
                    lstTMSExhibitAtt = dbHris.TMS_RT_Staff_eExhibitAtt.Where(x => x.Update_Flag == "0" && x.Status == "0").ToList();

                    List<TMS_RT_Staff_Attendance_History> lstTMSAttHistory = new List<TMS_RT_Staff_Attendance_History>();

                    foreach (var attendance in lstTMSExhibitAtt)
                    {
                        List<TMS_RT_Staff_eExhibitAtt> lstSubTMSAtt = new List<TMS_RT_Staff_eExhibitAtt>();
                        List<TMS_RT_Staff_Attendance_History> lstRTMSAttHistory = new List<TMS_RT_Staff_Attendance_History>();

                        //Get Staff Attendance History
                        lstSubTMSAtt = dbHris.TMS_RT_Staff_eExhibitAtt.Where(x => x.Employee_ID == attendance.Employee_ID && x.Payroll_ID == attendance.Payroll_ID && x.Exhibit_Date == attendance.Exhibit_Date && x.Status == "0").ToList();

                        //Remove Staff Attendance History
                        lstRTMSAttHistory = dbHris.TMS_RT_Staff_Attendance_History.Where(x => x.Employee_ID == attendance.Employee_ID && x.Payroll_ID == attendance.Payroll_ID && x.Attendance_Date == attendance.Exhibit_Date && x.Status == "0" && x.Created_By == ConstHelpers.Created_By.AUTO_eEXHIBIT.ToString()).ToList();

                        foreach (var attHistory in lstRTMSAttHistory)
                        {
                            attHistory.Status = "1";

                            new Hris_Tms().UpdateAttendanceHistory(attHistory, ref dbHris);
                        }

                        //Split eExhibit Attendance
                        foreach (var subAttendance in lstSubTMSAtt)
                        {
                            TMS_RT_Staff_Attendance_History objAttHistory = new TMS_RT_Staff_Attendance_History();

                            //Time In
                            objAttHistory.Region_ID = subAttendance.Region_ID;
                            objAttHistory.Payroll_ID = subAttendance.Payroll_ID;
                            objAttHistory.Employee_ID = subAttendance.Employee_ID;
                            objAttHistory.Attendance_Date = subAttendance.Exhibit_Date;
                            objAttHistory.Attendance_Time = subAttendance.Exhibit_CheckInTime.AddMilliseconds(-subAttendance.Exhibit_CheckInTime.TimeOfDay.Milliseconds).TimeOfDay;
                            objAttHistory.Machine_Name = "";
                            objAttHistory.Status = "0";
                            objAttHistory.Created_By = ConstHelpers.Created_By.AUTO_eEXHIBIT.ToString();
                            objAttHistory.Created_DateTime = DateTime.Now;
                            objAttHistory.RGUID = Guid.NewGuid();

                            lstTMSAttHistory.Add(objAttHistory);

                            objAttHistory = new TMS_RT_Staff_Attendance_History();

                            //Time Out
                            objAttHistory.Region_ID = subAttendance.Region_ID;
                            objAttHistory.Payroll_ID = subAttendance.Payroll_ID;
                            objAttHistory.Employee_ID = subAttendance.Employee_ID;
                            objAttHistory.Attendance_Date = subAttendance.Exhibit_Date;
                            objAttHistory.Attendance_Time = (((DateTime)subAttendance.Exhibit_CheckOutTime).AddMilliseconds(-((DateTime)subAttendance.Exhibit_CheckOutTime).TimeOfDay.Milliseconds)).TimeOfDay;
                            objAttHistory.Machine_Name = "";
                            objAttHistory.Status = "0";
                            objAttHistory.Created_By = ConstHelpers.Created_By.AUTO_eEXHIBIT.ToString();
                            objAttHistory.Created_DateTime = DateTime.Now;
                            objAttHistory.RGUID = Guid.NewGuid();

                            lstTMSAttHistory.Add(objAttHistory);
                        }

                        //Update eExhibit Flag
                        attendance.Update_Flag = "1";
                        attendance.Modified_By = ConstHelpers.Modified_By.AUTO_eEXHIBIT.ToString();
                        attendance.Modified_DateTime = DateTime.Now;

                        new Hris_Tms().UpdateExhibitionAtt(attendance, ref dbHris);
                    }

                    if (lstTMSAttHistory.Count > 0) { new Hris_Tms().InsertAttendanceHistory(lstTMSAttHistory, ref dbHris); }

                    dbHris.SaveChanges();
                    _dbTran.Commit();
                   }

                ReMapServiceEntity(serviceEntity.Country, serviceEntity.CompanyID, ConstHelpers.SystemName.HRIS.ToString(), true);

                Helper.RecordToLog("End " + ConstHelpers.ServiceName.SynceExhibitAttendance.ToString(), null);
            }
            catch (Exception ex)
            {
                GenerateErrorEmail("[" + ConstHelpers.EmailName.HRIS.ToString() + "] " + ConstHelpers.ServiceName.SynceExhibitAttendance.ToString(), serviceEntity.Country, serviceEntity.CompanyID, serviceEntity.BranchID, ConstHelpers.EmailName.HRIS.ToString(), ConstHelpers.ServiceName.SynceExhibitAttendance.ToString(), ex);

                Helper.RecordToLog(ConstHelpers.ServiceName.SynceExhibitAttendance.ToString(), ex);

                throw new ArgumentException(ConstHelpers.ServiceName.SynceExhibitAttendance.ToString() + " Error: " + ex.Message);
            }
        }

        public void SyncHRISAttendance()
        {
            try
            {
                Helper.RecordToLog("Start " + ConstHelpers.ServiceName.SyncHRISAttendance.ToString(), null);

                using (var _dbTran = dbHris.Database.BeginTransaction())
                {
                    new Hris_Tms().SyncHRISAttendance(serviceEntity, ref dbHris);
                    
                    dbHris.SaveChanges();
                    _dbTran.Commit();
                }

                Helper.RecordToLog("End " + ConstHelpers.ServiceName.SyncHRISAttendance.ToString(), null);
            }
            catch (Exception ex)
            {
                GenerateErrorEmail("[" + ConstHelpers.EmailName.HRIS.ToString() + "] " + ConstHelpers.ServiceName.SyncHRISAttendance.ToString(), serviceEntity.Country, serviceEntity.CompanyID, serviceEntity.BranchID, ConstHelpers.EmailName.HRIS.ToString(), ConstHelpers.ServiceName.SyncHRISAttendance.ToString(), ex);

                Helper.RecordToLog(ConstHelpers.ServiceName.SyncHRISAttendance.ToString(), ex);

                throw new ArgumentException(ConstHelpers.ServiceName.SyncHRISAttendance.ToString() + " Error: " + ex.Message);
            }
        }

        public void SyncPromoterCommIncentive()
        {
            try
            {
                Helper.RecordToLog("Start " + ConstHelpers.ServiceName.SyncPromoterCommIncentive.ToString(), null);

                ReMapServiceEntity(serviceEntity.Country, serviceEntity.CompanyID, ConstHelpers.SystemName.AES.ToString(), true);

                int monthDiff = GetMonthDifference((DateTime)serviceEntity.DateFrom, (DateTime)serviceEntity.DateTo);
                DateTime dateTo = (DateTime)serviceEntity.DateTo;

                if (monthDiff > 0)
                {
                    for (int diff = monthDiff ; diff >= 0; diff--)
                    {
                        DateTime newDate = Convert.ToDateTime(dateTo).AddMonths(-diff);
                        serviceEntity.DateTo = new DateTime(newDate.Year, newDate.Month, DateTime.DaysInMonth(newDate.Year, newDate.Month));

                        List<Exhibition_csp_ReadHrisCommIncentive> lstExhibitComm = new eExhibit().GetEventCommIncentive(serviceEntity);
                        List<STAFF_RT_Event_CommissonIncentive> lstCommIncentive = new List<STAFF_RT_Event_CommissonIncentive>();

                        using (var _dbTran = dbHris.Database.BeginTransaction())
                        {
                            foreach (var comm in lstExhibitComm)
                            {
                                ReMapServiceEntity(comm.Company_ID.Substring(0, 2), comm.Company_ID, ConstHelpers.SystemName.HRIS.ToString(), false);

                                //Check Existent
                                var staffComm = dbHris.STAFF_RT_Event_CommissonIncentive.Where(x => x.Region_ID == mappedCountry && x.Event_Company_ID == mappedCompany && x.Employee_ID == comm.HRIS_Employee_ID && x.Comm_Period == comm.Comm_Date && x.Status == false).FirstOrDefault();

                                if (staffComm != null)
                                {
                                    new Hris_Staff().DeleteEventStaffCommIncentive(staffComm, ref dbHris);
                                }

                                lstCommIncentive.Add(new Entities.STAFF_RT_Event_CommissonIncentive
                                {
                                    Region_ID = mappedCountry,
                                    Employee_ID = comm.HRIS_Employee_ID,
                                    Event_Company_ID = mappedCompany,
                                    Comm_Amount = comm.Comm_Amount,
                                    Incentive_Amount = comm.Incentive_Amount,
                                    Comm_Period = comm.Comm_Date,
                                    Created_By = ConstHelpers.Created_By.AUTO_eEXHIBIT.ToString(),
                                    Created_DateTime = DateTime.Now,
                                    Status = false,
                                    RGUID = Guid.NewGuid()
                                });
                            }

                            if (lstCommIncentive.Count > 0) { new Hris_Staff().InsertEventStaffCommIncentive(lstCommIncentive, ref dbHris); }

                            dbHris.SaveChanges();
                            _dbTran.Commit();
                        }
                    }
                    
                }

                Helper.RecordToLog("End " + ConstHelpers.ServiceName.SyncPromoterCommIncentive.ToString(), null);
            }
            catch (Exception ex)
            {
                GenerateErrorEmail("[" + ConstHelpers.EmailName.HRIS.ToString() + "] " + ConstHelpers.ServiceName.SyncPromoterCommIncentive.ToString(), serviceEntity.Country, serviceEntity.CompanyID, serviceEntity.BranchID, ConstHelpers.EmailName.HRIS.ToString(), ConstHelpers.ServiceName.SyncPromoterCommIncentive.ToString(), ex);

                Helper.RecordToLog(ConstHelpers.ServiceName.SyncPromoterCommIncentive.ToString(), ex);

                throw new ArgumentException(ConstHelpers.ServiceName.SyncPromoterCommIncentive.ToString() + " Error: " + ex.Message);
            }
        }

        public void SyncCalculatePromoterOT()
        {
            try
            {
                Helper.RecordToLog("Start " + ConstHelpers.ServiceName.SyncCalculatePromoterOT.ToString(), null);

                string message = new Hris_Tms().SyncCalculatePromoterOT(serviceEntity, ref dbHris);

                Helper.RecordToLog("End " + ConstHelpers.ServiceName.SyncCalculatePromoterOT.ToString(), null);
            }
            catch (Exception ex)
            {
                GenerateErrorEmail("[" + ConstHelpers.EmailName.HRIS.ToString() + "] " + ConstHelpers.ServiceName.SyncCalculatePromoterOT.ToString(), serviceEntity.Country, serviceEntity.CompanyID, serviceEntity.BranchID, ConstHelpers.EmailName.HRIS.ToString(), ConstHelpers.ServiceName.SyncCalculatePromoterOT.ToString(), ex);

                Helper.RecordToLog(ConstHelpers.ServiceName.SyncCalculatePromoterOT.ToString(), ex);

                throw new ArgumentException(ConstHelpers.ServiceName.SyncCalculatePromoterOT.ToString() + " Error: " + ex.Message);
            }
        }

        public void SyncServicePoints()
        {
            try
            {
                ReMapServiceEntity(serviceEntity.Country, serviceEntity.CompanyID, ConstHelpers.SystemName.AES.ToString(), false);

                switch (mappedCompany)
                {
                    case "SG01":
                    case "MY01":
                        dbAESBrand = new DBContexts.Contexts.AESBrandContext(mappedCountry, "NYSS");
                        break;
                    case "SG02":
                    case "MY02":
                        dbAESBrand = new DBContexts.Contexts.AESBrandContext(mappedCountry, "LWM");
                        break;
                    case "SG03":
                    case "MY03":
                        dbAESBrand = new DBContexts.Contexts.AESBrandContext(mappedCountry, "YN");
                        break;
                    case "SG05":
                    case "MY05":
                        dbAESBrand = new DBContexts.Contexts.AESBrandContext(mappedCountry, "DORRA");
                        break;
                    case "SG06":
                    case "MY06":
                        dbAESBrand = new DBContexts.Contexts.AESBrandContext(mappedCountry, "SKR");
                        break;
                    case "SG07":
                    case "MY07":
                        dbAESBrand = new DBContexts.Contexts.AESBrandContext(mappedCountry, "JS");
                        break;
                    case "SG08":
                    case "MY08":
                        dbAESBrand = new DBContexts.Contexts.AESBrandContext(mappedCountry, "VFL");
                        break;
                }

                List<AES_Staff_csp_ReadHRTrainingTransactions> lstServicePoints = new AES_Staff().GetServicePoints(ref dbAESBrand);

                foreach (var objServicePoint in lstServicePoints)
                {
                    STAFF_RT_Staff_ServicePoint servicePoint = new STAFF_RT_Staff_ServicePoint();

                    servicePoint.Employee_ID = objServicePoint.HRIS_Employee_ID;
                    servicePoint.Staff_ID = objServicePoint.Staff_ID;
                    servicePoint.Region_ID = serviceEntity.Country;
                    servicePoint.Company_ID = serviceEntity.CompanyID;
                    char[] delimiterChars = { '-' };
                    string[] result = objServicePoint.ToString().Split(delimiterChars);

                    var effectivefrom = new DateTime(Convert.ToInt32(objServicePoint.Trans_Year), Convert.ToInt32(objServicePoint.trans_Month), 1);
                    var effectiveto = new DateTime(Convert.ToInt32(objServicePoint.Trans_Year), Convert.ToInt32(objServicePoint.trans_Month), DateTime.DaysInMonth(Convert.ToInt32(objServicePoint.Trans_Year), Convert.ToInt32(objServicePoint.trans_Month)));
                    servicePoint.Effective_From = effectivefrom;
                    servicePoint.Effective_To = effectiveto;
                    if (objServicePoint.ServicePoint ==  null)
                    {
                        objServicePoint.ServicePoint = 0;
                    }
                    servicePoint.Service_Point = objServicePoint.ServicePoint;
                    servicePoint.Status = "0";
                    servicePoint.Created_By = "Admin";
                    servicePoint.Created_DateTime = DateTime.Now;
                    servicePoint.Modified_By = "Admin";
                    servicePoint.Modified_DateTime = DateTime.Now;
                    new Hris_Staff().InsertServicePoint(servicePoint, ref dbHris);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private double GetDistanceByCoordinates(string exhibitionCoordinates, string checkInCoordinates)
        {
            try
            {
                string checkInLocation = checkInCoordinates;
                string exhibitionLocation = exhibitionCoordinates;

                string[] arrCheckIn = checkInLocation.Split(';');
                string[] arrExhibition = exhibitionLocation.Split(';');

                double lat1 = Convert.ToDouble(arrCheckIn[0]);
                double lon1 = Convert.ToDouble(arrCheckIn[1]);
                double lat2 = Convert.ToDouble(arrExhibition[0]);
                double lon2 = Convert.ToDouble(arrExhibition[1]);

                var earthRadiusInMeter = 6378137;

                var dLat = ConvertDegreeToRadians((lat2 - lat1).ToString());
                var dLon = ConvertDegreeToRadians((lon2 - lon1).ToString());

                lat1 = Convert.ToDouble(ConvertDegreeToRadians(lat1.ToString()));
                lat2 = Convert.ToDouble(ConvertDegreeToRadians(lat2.ToString()));

                var a = Math.Sin(Convert.ToDouble(dLat / 2)) * Math.Sin(Convert.ToDouble(dLat / 2)) +
                        Math.Sin(Convert.ToDouble(dLon / 2)) * Math.Sin(Convert.ToDouble(dLon / 2)) * Math.Cos(lat1) * Math.Cos(lat2);

                var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));

                double distance = earthRadiusInMeter * c;

                return distance;
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("GetDistanceByCoordinates", ex);

                throw new ArgumentException("GetDistanceByCoordinates Error: " + ex.Message);
            }          
        }

        private double ConvertDegreeToRadians(string degrees)
        {
            try
            {
                return Convert.ToDouble(degrees) * Math.PI / 180;
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("ConvertDegreeToRadians", ex);

                throw new ArgumentException("ConvertDegreeToRadians Error: " + ex.Message);
            }
        }

        public void GenerateErrorEmail(string fromName, string country, string brand, string branch, string system, string service, Exception ex)
        {
            try
            {
                string message = string.Empty;

                message = "Exception Message: " + ex.Message;

                if (ex.InnerException != null)
                {
                    message = message + " Inner Exception Message: " + ex.InnerException.Message;
                }

                emailEntity.ErrorSubject = ReplaceEmail(emailEntity.ErrorSubject, country, brand, branch, system, service, "");
                emailEntity.ErrorBody = ReplaceEmail(emailEntity.ErrorBody, country, brand, branch, system, service, message);

                string[] lstEmailError = emailEntity.ErrorToEmail.Split(';');

                foreach(var email in lstEmailError)
                {
                   Hris_STAFF_T_Staff objStaff = new Hris_Staff().GetStaffByEmployeeID(email, ref dbHris);

                    emailEntity.ErrorToEmail = string.Empty;

                    if (objStaff != null)
                    {
                        if (!String.IsNullOrEmpty(emailEntity.ErrorToEmail))
                        {
                            emailEntity.ErrorToEmail = emailEntity.ErrorToEmail + ";";
                        }

                        emailEntity.ErrorToEmail = emailEntity.ErrorToEmail + objStaff.Staff_Email;
                    }
                }

                eMail.SendEmail("", fromName, true, emailEntity);
            }
            catch (Exception exmsg)
            {
                Helper.RecordToLog("GenerateErrorEmail", ex);

                throw new ArgumentException("GenerateErrorEmail Error: " + exmsg.Message);
            }
        }

        public string ReplaceEmail(string email, string country, string company, string branch, string system, string service, string message)
        {
            email = email.Replace("[Country]", String.IsNullOrEmpty(country) ? "ALL" : country);
            email = email.Replace("[Company]", String.IsNullOrEmpty(company) ? "ALL" : company);
            email = email.Replace("[Branch]", String.IsNullOrEmpty(branch) ? "ALL" : branch);
            email = email.Replace("[System]", system);
            email = email.Replace("[Service]", service);
            email = email.Replace("[Message]", message);
            email = email.Replace("{", "<");
            email = email.Replace("}", ">");

            return email;
        }

        public void PopulateEmail(Hashtable hashtable)
        {
            try
            {
                emailEntity.Subject = emailEntity.Subject.Replace("[Brand]", hashtable["Brand"].ToString());
                emailEntity.Subject = emailEntity.Subject.Replace("[Date]", hashtable["Date"].ToString());
                emailEntity.Subject = emailEntity.Subject.Replace("{", "<");
                emailEntity.Subject = emailEntity.Subject.Replace("}", ">");
                emailEntity.Body = emailEntity.Body.Replace("{", "<");
                emailEntity.Body = emailEntity.Body.Replace("}", ">");
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("PopulateEmail", ex);

                throw new ArgumentException("PopulateEmail Error: " + ex.Message);
            }
        }

        public void ReMapServiceEntity(string paramCountry, string paramCompany, string module, bool changeEntityFlag)
        {
            try
            {
                List<SETTINGS_T_Application_Mapping> lstApplicationMapping = new List<SETTINGS_T_Application_Mapping>();
                lstApplicationMapping = new Hris_Settings().GetApplicationMapping();

                switch (module)
                {
                    case "AES":
                        if (paramCountry == ConstHelpers.Country.SIN.ToString())
                        {
                            if (changeEntityFlag)
                            {
                                serviceEntity.Country = ConstHelpers.Country.SG.ToString();
                            }

                            mappedCountry = ConstHelpers.Country.SG.ToString();
                        }
                        if (paramCountry == ConstHelpers.Country.MYY.ToString())
                        {
                            if (changeEntityFlag)
                            {
                                serviceEntity.Country = ConstHelpers.Country.MY.ToString();
                            }

                            mappedCountry = ConstHelpers.Country.MY.ToString();
                        }

                        if (!String.IsNullOrEmpty(serviceEntity.CompanyID))
                        {
                            if (changeEntityFlag)
                            {
                                serviceEntity.CompanyID = lstApplicationMapping.Where(x => x.Mapped_To == serviceEntity.CompanyID && x.Field_Type == "COMPANY_ID" && x.System_ID == "EXHIBITION" && x.Action_Type == "PULL" && x.Status == "0").Select(x => x.Mapped_From).FirstOrDefault();
                            }

                            mappedCompany = lstApplicationMapping.Where(x => x.Mapped_To == serviceEntity.CompanyID && x.Field_Type == "COMPANY_ID" && x.System_ID == "EXHIBITION" && x.Action_Type == "PULL" && x.Status == "0").Select(x => x.Mapped_From).FirstOrDefault();
                        }
                        break;
                    case "HRIS":
                        if (paramCountry == ConstHelpers.Country.SG.ToString())
                        {
                            if (changeEntityFlag)
                            {
                                serviceEntity.Country = ConstHelpers.Country.SIN.ToString();
                            }

                            mappedCountry = ConstHelpers.Country.SIN.ToString();
                        }
                        if (paramCountry == ConstHelpers.Country.MY.ToString())
                        {
                            if (changeEntityFlag)
                            {
                                serviceEntity.Country = ConstHelpers.Country.MYY.ToString();
                            }

                            mappedCountry = ConstHelpers.Country.MYY.ToString();
                        }

                        if (!String.IsNullOrEmpty(serviceEntity.CompanyID))
                        {
                            if (changeEntityFlag)
                            {
                                serviceEntity.CompanyID = lstApplicationMapping.Where(x => x.Mapped_From == serviceEntity.CompanyID && x.Field_Type == "COMPANY_ID" && x.System_ID == "EXHIBITION" && x.Action_Type == "PULL" && x.Status == "0").Select(x => x.Mapped_To).FirstOrDefault();
                            }

                            mappedCompany = lstApplicationMapping.Where(x => x.Mapped_From == serviceEntity.CompanyID && x.Field_Type == "COMPANY_ID" && x.System_ID == "EXHIBITION" && x.Action_Type == "PULL" && x.Status == "0").Select(x => x.Mapped_To).FirstOrDefault();
                        }
                        else if (!String.IsNullOrEmpty(paramCompany))
                        {
                            mappedCompany = lstApplicationMapping.Where(x => x.Mapped_From == paramCompany && x.Field_Type == "COMPANY_ID" && x.System_ID == "EXHIBITION" && x.Action_Type == "PULL" && x.Status == "0").Select(x => x.Mapped_To).FirstOrDefault();
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("PopulateEmail", ex);

                throw new ArgumentException("PopulateEmail Error: " + ex.Message);
            }
        }

        public static int GetMonthDifference(DateTime startDate, DateTime endDate)
        {
            int monthsApart = 12 * (startDate.Year - endDate.Year) + startDate.Month - endDate.Month;
            return Math.Abs(monthsApart);
        }
    }

}
