﻿using System;
using System.Net;
using System.Net.Mail;
using eHRISService.Entities;

namespace eHRISService
{
    class eMail
    {
        public static void SendEmail(string fileName, string fromName, bool errorFlag, EmailEntity emailEntity)
        {
            try
            {
                string toEmail = string.Empty;
                string ccEmail = string.Empty;
                string bccEmail = string.Empty;

                var msg = new MailMessage();
                var smtp = new SmtpClient(emailEntity.SmtpServer);
                var creds = new NetworkCredential(emailEntity.SmtpLogin, emailEntity.SmtpPassword);

                if (errorFlag)
                {
                    toEmail = emailEntity.ErrorToEmail;
                    msg.Subject = emailEntity.ErrorSubject;
                    msg.Body = emailEntity.ErrorBody;
                }
                else
                {
                    toEmail = emailEntity.ToEmail;
                    ccEmail = emailEntity.CcEmail;
                    bccEmail = emailEntity.BccEmail;
                    msg.Subject = emailEntity.Subject;
                    msg.Body = emailEntity.Body;
                }

                if (emailEntity.SmtpPort != null)
                {
                    smtp.Port = (int)emailEntity.SmtpPort;
                }

                smtp.UseDefaultCredentials = true;
                smtp.Credentials = creds;
                smtp.EnableSsl = emailEntity.SmtpSSL == 0 ? false : true;

                string[] tmpToEmail = toEmail.Split(';');
                string[] tmpCcEmail = ccEmail.Split(';');

                if (toEmail != "")
                {
                    foreach (var subTo in tmpToEmail)
                    {
                        msg.To.Add(subTo);
                    }
                }

                if (ccEmail != "")
                {
                    foreach (var subCc in tmpCcEmail)
                    {
                        msg.CC.Add(subCc);
                    }
                }

                msg.From = new MailAddress(emailEntity.FromEmail, fromName);
                msg.IsBodyHtml = true;

                if (fileName != "")
                {
                    msg.Attachments.Add(new Attachment(fileName));
                }

                smtp.Send(msg);
                msg.Attachments.Dispose();
            }
            catch (Exception ex)
            {
                throw new ArgumentException("SendEmail Error: " + ex.Message + " Inner Exception: " + ex.InnerException != null ? ex.InnerException.Message : "-");
            }
           
        }
    }
}
