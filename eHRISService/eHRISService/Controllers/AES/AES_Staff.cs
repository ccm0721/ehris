﻿using eHRISService.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eHRISService.Controllers.AES
{
    public class AES_Staff
    {
        public List<AES_Staff_csp_ReadHRTrainingTransactions> GetServicePoints(ref DBContexts.Contexts.AESBrandContext dbAES)
        {
            var sql = @"Staff.csp_ReadHRTrainingTransactions";
            List<SqlParameter> parameterList = new List<SqlParameter>();
            SqlParameter[] parameters = parameterList.ToArray();

            List<AES_Staff_csp_ReadHRTrainingTransactions> CommResult;
            CommResult = dbAES.Database.SqlQuery<AES_Staff_csp_ReadHRTrainingTransactions>(sql, parameters).ToList();
            return CommResult;
        }

    }

}
