﻿using eHRISService.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eHRISService.Controllers.Hris
{
    public class Hris_Staff
    {
        public Hris_STAFF_T_Staff GetStaffByEmployeeID(string employeeID, ref DBContexts.Contexts.HrisContext dbHris)
        {
            try
            {
               Hris_STAFF_T_Staff objStaff = dbHris.STAFF_T_Staff.Where(x => x.Employee_ID == employeeID && x.Status == "0").FirstOrDefault();

                return objStaff;
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("GetStaffByEmployeeID", ex);
                throw ex;
            }
        }

        public string InsertEventStaffCommIncentive(List<STAFF_RT_Event_CommissonIncentive> lstCommIncentive, ref DBContexts.Contexts.HrisContext dbHris)
        {
            try
            {
                dbHris.STAFF_RT_Event_CommissonIncentive.AddRange(lstCommIncentive);
                dbHris.SaveChanges();

                return "Insert Event Staff Commission Incentive Successfully";
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("InsertEventStaffCommIncentive", ex);
                throw ex;
            }
        }

        public string DeleteEventStaffCommIncentive(STAFF_RT_Event_CommissonIncentive commIncentive, ref DBContexts.Contexts.HrisContext dbHris)
        {
            try
            {
                dbHris.STAFF_RT_Event_CommissonIncentive.Remove(commIncentive);
                dbHris.SaveChanges();

                return "Delete Event Staff Commission Incentive Successfully";
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("DeleteEventStaffCommIncentive", ex);
                throw ex;
            }
        }

        public int InsertServicePoint(STAFF_RT_Staff_ServicePoint objServicePoint, ref DBContexts.Contexts.HrisContext dbHris)
        {
            STAFF_RT_Staff_ServicePoint item = dbHris.STAFF_RT_Staff_ServicePoint.Where(x => x.Employee_ID == objServicePoint.Employee_ID && x.Effective_From == objServicePoint.Effective_From && x.Effective_To == objServicePoint.Effective_To).FirstOrDefault();
            if (item != null && item.SNO > 0)
            {
                item.Service_Point = objServicePoint.Service_Point;
                objServicePoint.SNO = item.SNO;

                dbHris.Entry(objServicePoint).State = EntityState.Modified;
                dbHris.SaveChanges();
            }
            else
            {
                dbHris.STAFF_RT_Staff_ServicePoint.Add(objServicePoint);
                dbHris.SaveChanges();
            }

            return objServicePoint.SNO;
        }
    }
}
