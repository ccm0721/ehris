﻿using eHRISService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eHRISService.Controllers.Hris
{
   public class Hris_Settings
    {
        private DBContexts.Contexts.HrisContext dbHris = new DBContexts.Contexts.HrisContext();

        public List<SETTINGS_T_Application_Mapping> GetApplicationMapping()
        {
            try
            {
                List<SETTINGS_T_Application_Mapping> lstApplicationMapping = new List<SETTINGS_T_Application_Mapping>();

                lstApplicationMapping = dbHris.SETTINGS_T_Application_Mapping.Where(x => x.Status == "0").ToList();

                return lstApplicationMapping;
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("GetApplicationMapping", ex);
                throw ex;
            }
        }

        public List<SETTINGS_RT_Scheduler_Config> GetSchedulerConfigById(int Id, ref DBContexts.Contexts.HrisContext dbHris)
        {
            try
            {
                dbHris.Database.CommandTimeout = 300;
                List<SETTINGS_RT_Scheduler_Config> lstSchedulerConfig = dbHris.Database.SqlQuery<SETTINGS_RT_Scheduler_Config>("EXEC SETTINGS.csp_GetSettingsSchedulerById @Scheduler_ID = {0}", Id).ToList();

                return lstSchedulerConfig;
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("GetSchedulerConfigById", ex);
                throw ex;
            }
        }

        public SETTINGS_T_System_Parameter GetSystemParameterByName(string regionId, string parameterName, ref DBContexts.Contexts.HrisContext dbHris)
        {
            try
            {
                SETTINGS_T_System_Parameter objSystemParameter = dbHris.SETTINGS_T_System_Parameter.Where(x => x.Region_ID == regionId && x.Parameter_Name == parameterName && DateTime.Now.Date >= x.Effective_From && DateTime.Now.Date <= x.Effective_To && x.Status == "0").FirstOrDefault();

                return objSystemParameter;
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("GetSystemParameterByName", ex);
                throw ex;
            }
        }
    }
}
