﻿using eHRISService.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eHRISService.Controllers.Hris
{
    public class Hris_Audit
    {
        public string PostNotificationLog(AUDIT_T_Notification_Log notificationLog, ref DBContexts.Contexts.HrisContext dbHris)
        {
            try
            {
                dbHris.AUDIT_T_Notification_Log.Add(notificationLog);
                dbHris.SaveChanges();

                return "Insert Attendance History Successfully";
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("PostNotificationLog", ex);
                throw ex;
            }
        }
    }

}
