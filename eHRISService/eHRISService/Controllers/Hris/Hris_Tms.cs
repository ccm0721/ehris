﻿using eHRISService.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eHRISService.Controllers.Hris
{
    public class Hris_Tms
    {
        public string InsertExhibition(List<TMS_T_Exhibition> lstExhibition, ref DBContexts.Contexts.HrisContext dbHris)
        {
            try
            {
                dbHris.TMS_T_Exhibition.AddRange(lstExhibition);
                dbHris.SaveChanges();

                return "Insert Exhibition Master Successfully";
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("InsertExhibition", ex);
                throw ex;
            }
        }

        public string UpdateExhibition(TMS_T_Exhibition objExhibition, ref DBContexts.Contexts.HrisContext dbHris)
        {
            try
            {
                dbHris.Entry(objExhibition).State = EntityState.Modified;
                dbHris.SaveChanges();

                return "Update Exhibition Master Successfully";
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("UpdateExhibition", ex);
                throw ex;
            }
        }

        public string InsertExhibitionAtt(List<TMS_RT_Staff_eExhibitAtt> lstAttendance, ref DBContexts.Contexts.HrisContext dbHris)
        {
            try
            {
                dbHris.TMS_RT_Staff_eExhibitAtt.AddRange(lstAttendance);
                dbHris.SaveChanges();

                return "Insert eExhibit Attendance Successfully";
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("InsertExhibitionAtt", ex);
                throw ex;
            }
        }

        public string UpdateExhibitionAtt(TMS_RT_Staff_eExhibitAtt objAttendance, ref DBContexts.Contexts.HrisContext dbHris)
        {
            try
            {
                dbHris.Entry(objAttendance).State = EntityState.Modified;
                dbHris.SaveChanges();

                return "Update eExhibit Attendance Successfully";
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("UpdateExhibitionAtt", ex);
                throw ex;
            }
        }

        public string InsertAttendanceHistory(List<TMS_RT_Staff_Attendance_History> lstAttHistory, ref DBContexts.Contexts.HrisContext dbHris)
        {
            try
            {
                dbHris.TMS_RT_Staff_Attendance_History.AddRange(lstAttHistory);
                dbHris.SaveChanges();

                return "Insert Attendance History Successfully";
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("InsertAttendanceHistory", ex);
                throw ex;
            }
        }

        public string UpdateAttendanceHistory(TMS_RT_Staff_Attendance_History objAttHistory, ref DBContexts.Contexts.HrisContext dbHris)
        {
            try
            {
                dbHris.Entry(objAttHistory).State = EntityState.Modified;
                dbHris.SaveChanges();

                return "Update Attendance History Successfully";
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("UpdateAttendanceHistory", ex);
                throw ex;
            }
        }

        public string SyncHRISAttendance(ServiceEntity objSearch, ref DBContexts.Contexts.HrisContext dbHris)
        {
            try
            {
                dbHris.Database.CommandTimeout = 900;
                dbHris.Database.ExecuteSqlCommand("EXEC TMS.csp_SyncAttendance @Region_ID = {0}, @Payroll_ID = {1}, @Employee_ID = {2}, @Start_Attendance_Date = {3}, @End_Attendance_Date = {4}, @Check_Type = {5}", objSearch.Country, objSearch.PayrollID, objSearch.EmployeeID, objSearch.DateFrom, objSearch.DateTo, objSearch.CheckType);

                return "Sync HRISAttendance Successfully";
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("SyncHRISAttendance", ex);
                throw ex;
            }
        }

        public List<TMS_csp_GetStaffAttendanceNotification> GetStaffAttendanceNotification(ServiceEntity objSearch, ref DBContexts.Contexts.HrisContext dbHris)
        {
            try
            {
                var sql = @"TMS.csp_GetStaffAttendanceNotification  @Region_ID, @Company_ID, @Branch_ID, @Company_Type, @Effective_From, @Effective_To, @Exception_List";
                List<SqlParameter> parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter("Region_ID", String.IsNullOrEmpty(objSearch.Country) ? "" : objSearch.Country));
                parameterList.Add(new SqlParameter("Company_ID", String.IsNullOrEmpty(objSearch.CompanyID) ? "" : objSearch.CompanyID));
                parameterList.Add(new SqlParameter("Branch_ID", String.IsNullOrEmpty(objSearch.BranchID) ? "" : objSearch.BranchID));
                parameterList.Add(new SqlParameter("Company_Type", String.IsNullOrEmpty(objSearch.CompanyType) ? "" : objSearch.CompanyType));
                parameterList.Add(new SqlParameter("Effective_From", objSearch.DateFrom == null ? null : objSearch.DateFrom));
                parameterList.Add(new SqlParameter("Effective_To", objSearch.DateTo == null ? null : objSearch.DateTo));
                parameterList.Add(new SqlParameter("Exception_List", objSearch.Notification_Exception == null ? "" : objSearch.Notification_Exception));
                SqlParameter[] parameters = parameterList.ToArray();

                List<TMS_csp_GetStaffAttendanceNotification> lstAttendance;

                lstAttendance = dbHris.Database.SqlQuery< TMS_csp_GetStaffAttendanceNotification>(sql, parameters).ToList();
                return lstAttendance;
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("GetStaffAttendanceNotification", ex);
                throw ex;
            }
        }

        public string SyncCalculatePromoterOT(ServiceEntity objSearch, ref DBContexts.Contexts.HrisContext dbHris)
        {
            try
            {
                dbHris.Database.CommandTimeout = 600;
                dbHris.Database.ExecuteSqlCommand("EXEC TMS.csp_CalculateTmsPromoterOT @Region_ID = {0}, @Company_ID = {1}, @Branch_ID = {2}, @Employee_ID = {3}, @User_Employee_ID = {4}, @Effective_From = {5}, @Effective_To = {6}", objSearch.Country, objSearch.CompanyID, objSearch.BranchID, objSearch.EmployeeID, "AUTO_SYNC", objSearch.DateFrom, objSearch.DateTo);

                return "Sync CalculatePromoterOT Successfully";
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("SyncCalculatePromoterOT", ex);
                throw ex;
            }
        }
    }
}
