﻿using eHRISService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eHRISService.Controllers.eExhibit
{
    public class eExhibit
    {
        private DBContexts.Contexts.ePaymentContext dbePayment = new DBContexts.Contexts.ePaymentContext("SG");
        private DBContexts.Contexts.AESMasterContext dbAESMaster = new DBContexts.Contexts.AESMasterContext("SG");
        private DBContexts.Contexts.AESBusinessContext dbAESBusiness = new DBContexts.Contexts.AESBusinessContext("SG");
        private DBContexts.Contexts.eExhibitionContext dbeExhibition = new DBContexts.Contexts.eExhibitionContext("SG");

        public List<Exhibition_T_Exhibition> GetExhibitionMaster(ServiceEntity objSearch)
        {
            try
            {
                dbAESBusiness = new DBContexts.Contexts.AESBusinessContext(objSearch.Country);

                List<Exhibition_T_Exhibition> lstExhibition = new List<Exhibition_T_Exhibition>();

                DateTime filterDate = ((DateTime)objSearch.DateFrom).Date;

                lstExhibition = dbAESBusiness.Exhibition_T_Exhibition.Where(x => x.Company_ID == (String.IsNullOrEmpty(objSearch.CompanyID) ? x.Company_ID : objSearch.CompanyID)
                                                                            && (x.Created_DateTime >= filterDate || x.Modified_DateTime >= filterDate)).ToList();

                return lstExhibition;
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("GetExhibitionMaster", ex);
                throw ex;
            }
        }

        public List<Exhibition_Exhibition_Attendance> GetExhibitionAttendance(ServiceEntity objSearch)
        {
            try
            {
                dbePayment = new DBContexts.Contexts.ePaymentContext(objSearch.Country);

                List<Exhibition_Exhibition_Attendance> lstExhibitionAtt = new List<Exhibition_Exhibition_Attendance>();

                DateTime filterDate = ((DateTime)objSearch.DateFrom).Date;

                lstExhibitionAtt = dbePayment.Exhibition_Exhibition_Attendance.Where(x => x.BrandID == (String.IsNullOrEmpty(objSearch.CompanyID) ? x.BrandID : objSearch.CompanyID)
                                                                                     && (x.CreatedDate >= filterDate || x.UpdatedDate >= filterDate)).ToList();

                return lstExhibitionAtt;
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("GetExhibitionAttendance", ex);
                throw ex;
            }
        }

        public List<Exhibition_csp_ReadHrisCommIncentive> GetEventCommIncentive(ServiceEntity objSearch)
        {
            try
            {
                dbeExhibition = new DBContexts.Contexts.eExhibitionContext(objSearch.Country);

                List<Exhibition_csp_ReadHrisCommIncentive> lstCommIncentive = dbeExhibition.Database.SqlQuery<Exhibition_csp_ReadHrisCommIncentive>("EXEC Exhibition.csp_ReadHrisCommIncentive @Region_ID = {0}, @Company_ID = {1}, @Employee_ID = {2}, @Comm_Date = {3} ", objSearch.Country, objSearch.CompanyID, objSearch.EmployeeID, objSearch.DateTo).ToList();

                return lstCommIncentive;
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("GetEventCommIncentive", ex);
                throw ex;
            }
        }

    }

}
