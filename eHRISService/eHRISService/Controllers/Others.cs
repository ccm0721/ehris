﻿using eHRISService.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace eHRISService.Controllers.Hris
{
    public class Others
    {
        public HttpResponseMessage PostAzureNotification(AzureNotificationEntity azureNotification, string hub, string oAuthApi)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(oAuthApi);
                client.Timeout = TimeSpan.FromMilliseconds(Timeout.Infinite);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = new HttpResponseMessage();

                response = client.PostAsJsonAsync(hub, azureNotification).Result;

                return response;
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("PostAzureNotification", ex);
                throw ex;
            }
        }
    }

}
