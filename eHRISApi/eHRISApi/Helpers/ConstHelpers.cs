﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eHRISApi.Helpers
{
    public class ConstHelpers
    {
        public enum eCountry { SIN = 1, MYY = 2 }    
        public enum eExhibitReportCategory { PromoterComm = 1 }
        public enum MarcomReportType { PromoterComm = 1, PromoterCommDetails = 2 }
        public enum ReportFileType { Excel = 1, PDF = 2 }
    }
}