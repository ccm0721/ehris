﻿using eHRISApi.Entities;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace eHRISApi.DBContexts
{
   public class Contexts
    {
     
        public class HrisContext : DbContext
        {
            public HrisContext()
                : base("name=HRISEntities")
            {
            }
        }

        public class eExhibitionContext : DbContext
        {
            public eExhibitionContext(string country)
                : base("name=" + country + "eExhibitionEntities")
            {
            }
        }
    }
}
