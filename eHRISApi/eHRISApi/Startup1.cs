﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;

/* -- Add New Project, Empty Web Api
 * Install-Package Microsoft.AspNet.WebApi.Owin
 * Install-Package Microsoft.Owin.Host.SystemWeb
 * -- Add Owin "Startup" Class
 * -- Add WebApiConfig under "App_Start" folder
 * Install-Package Microsoft.Owin.Security.OAuth
 * Install-Package Microsoft.Owin.Cors
*/

[assembly: OwinStartup(typeof(eHRISApi.Startup1))]
namespace eHRISApi {
    public class Startup1 {
        public void Configuration(IAppBuilder app) {
            HttpConfiguration config = new HttpConfiguration();

            ConfigureOAuth(app);

            WebApiConfig.Register(config);
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(config);

        }

        private void ConfigureOAuth(IAppBuilder app) {
            OAuthBearerAuthenticationOptions OAuthBearerOptions = new OAuthBearerAuthenticationOptions();
            //Token Consumption
            app.UseOAuthBearerAuthentication(OAuthBearerOptions);
        }
    }
}
