﻿using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using eHRISApi.Entities;
using NPOI.SS.Util;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using System.Data;

// eSMSeApptApi
namespace eHRISApi {

    public interface IEntityStandard {
        int Id { get; set; }
        DateTime AddDate { get; set; }
        DateTime? ModDate { get; set; }
        [MaxLength(50)]
        string AddBy { get; set; }
        [MaxLength(50)]
        string ModBy { get; set; }
        bool Disabled { get; set; }
    }

    public class Helper {

        public static void setPostData(IEntityStandard entity, System.Security.Principal.IPrincipal user) {
            var currDate = System.DateTime.Now;
            entity.AddDate = currDate;
            //entity.ModDate = currDate;
            entity.AddBy = user.Identity.Name;
            entity.Disabled = false;
            //entity.ModBy = user.Identity.Name;
        }

        public static void setPutData(IEntityStandard entity, System.Security.Principal.IPrincipal user) {
            entity.ModDate = System.DateTime.Now;
            entity.ModBy = user.Identity.Name;
        }

        public static void setDeleteData(IEntityStandard entity, System.Security.Principal.IPrincipal user)
        {
            entity.Disabled = true;
            entity.ModDate = System.DateTime.Now;
            entity.ModBy = user.Identity.Name;
        }

        public static void setPostData(IEntityStandard entity, string StaffId)
        {
            var currDate = System.DateTime.Now;
            entity.AddDate = currDate;
            entity.ModDate = currDate;
            entity.AddBy = StaffId;
            entity.ModBy = StaffId;
        }
        public static void setPutData(IEntityStandard entity, string UserId)
        {
            entity.ModDate = System.DateTime.Now;
            entity.ModBy = UserId;
        }

        public static IQueryable<T> paginationQueryGeneration<T>(PageSettingViewEntity pageSetting, IQueryable<T> queryDataSet) {
            if (pageSetting.filterParams != null) {
                if (pageSetting.filterParams.Length > 0) {
                    foreach (var r in pageSetting.filterParams) {
                        switch (r.ops) {
                            case "==":
                                queryDataSet = queryDataSet.Where(r.key + ".ToString().ToUpper().Contains(@0)", r.value.ToUpper());
                                break;
                            case "32":
                            case "64":
                                if (r.value.EndsWith("00.000Z"))
                                    queryDataSet = queryDataSet.Where(r.key + " >= @0", Convert.ToDateTime(r.value));
                                else
                                    queryDataSet = queryDataSet.Where(r.key + " >= " + r.value);
                                break;
                            case "128":
                            case "256":
                                if (r.value.EndsWith("00.000Z"))
                                    queryDataSet = queryDataSet.Where(r.key + " <= @0", Convert.ToDateTime(r.value));  //.AddDays(1)
                                else
                                    queryDataSet = queryDataSet.Where(r.key + " <= " + r.value);
                                break;
                            //default:
                            //    queryDataSet = queryDataSet.Where(r.key + ".ToString().Contains(@0)", r.value);
                            //    break;
                        }
                    }
                }
            }
            //queryDataSet = queryDataSet.Where( pageSetting.filterParams );
            if (String.IsNullOrEmpty(pageSetting.sortOrder)) {
                try {
                    queryDataSet = queryDataSet.OrderBy("AddDate DESC").OrderBy("ModDate DESC");
                } catch (Exception ) {
                    queryDataSet = queryDataSet.OrderBy("Id DESC");
                }
            } else {
                queryDataSet = queryDataSet.OrderBy(pageSetting.sortColumnName + " " + pageSetting.sortOrder);
            }
            return queryDataSet;
        }

        public static void RecordToLog(string message, Exception ex)
        {
            StreamWriter log = null;
            string Path = AppDomain.CurrentDomain.BaseDirectory + "\\LogFile";
            string FileName = Path + "eHRISApi_log_" + DateTime.Today.ToString("dd-MM-yyyy") + ".txt";
            string LogMessage = string.Empty;
            try
            {
                System.IO.Directory.CreateDirectory(Path);
                if (!File.Exists(FileName))
                {
                    log = new StreamWriter(FileName);
                }
                else
                {
                    log = File.AppendText(FileName);
                }

                if (ex != null)
                {
                    LogMessage = "Error: " + ex.Message;

                    if (ex.InnerException != null)
                    {
                        LogMessage = LogMessage + " Inner Exception: "  + ex.InnerException.Message;
                    }
                }
                else
                {
                    LogMessage = message;
                }

                log.WriteLine(DateTime.Now + " | " + LogMessage);
                log.WriteLine();
            }
            catch (Exception etc)
            {
                Console.WriteLine(etc.Message);
            }
            finally
            {
                log.Close();
            }
        }
    }

    public class EmailService
    {

    }

    public class ExcelNPOIHelper
    {
        public struct stNPOIPageSetup
        {
            public Double HeaderMargin, FooterMargin, RightMargin,
                    TopMargin, LeftMargin, BottomMargin;
            public Boolean SetZoom, FitToPage, Landscape;
            public short ZoomPercent, FitHeight, FitWidth;
            public String PaperSize;
        }

        public stNPOIPageSetup ExcelNPOIDefaultPageSetup()
        {
            stNPOIPageSetup myPageSetup = new stNPOIPageSetup();

            myPageSetup.HeaderMargin = 0;
            myPageSetup.FooterMargin = 0;
            myPageSetup.RightMargin = 0;
            myPageSetup.TopMargin = 0;
            myPageSetup.LeftMargin = 0;
            myPageSetup.BottomMargin = 0;
            myPageSetup.PaperSize = "A4";
            myPageSetup.SetZoom = true;
            myPageSetup.ZoomPercent = 100;
            myPageSetup.FitToPage = false;
            myPageSetup.FitHeight = 99;
            myPageSetup.FitWidth = 1;
            myPageSetup.Landscape = false;

            return myPageSetup;
        }

        public HSSFWorkbook ExcelNPOICreate(string _SheetName)
        {
            var WBook = new HSSFWorkbook();

            ISheet WSheet = WBook.CreateSheet(_SheetName);

            var PageSetup = new stNPOIPageSetup();
            PageSetup = ExcelNPOIDefaultPageSetup();
            ExcelNPOIPageSetup(PageSetup, WBook);
            return WBook;
        }

        public void ExcelNPOIPageSetup(stNPOIPageSetup _PageSetup, HSSFWorkbook _WBook)
        {
            ISheet WorkSheet = _WBook.GetSheetAt(0);
            WorkSheet.IsPrintGridlines = false;
            WorkSheet.PrintSetup.Landscape = _PageSetup.Landscape;

            if (_PageSetup.HeaderMargin > 0) WorkSheet.PrintSetup.HeaderMargin = _PageSetup.HeaderMargin;
            if (_PageSetup.FooterMargin > 0) WorkSheet.PrintSetup.FooterMargin = _PageSetup.FooterMargin;
            if (_PageSetup.LeftMargin > 0) WorkSheet.SetMargin(MarginType.LeftMargin, _PageSetup.LeftMargin);
            if (_PageSetup.RightMargin > 0) WorkSheet.SetMargin(MarginType.RightMargin, _PageSetup.RightMargin);
            if (_PageSetup.TopMargin > 0) WorkSheet.SetMargin(MarginType.TopMargin, _PageSetup.TopMargin);
            if (_PageSetup.BottomMargin > 0) WorkSheet.SetMargin(MarginType.BottomMargin, _PageSetup.BottomMargin);

            switch (_PageSetup.PaperSize)
            {
                case "A4":
                    {
                        WorkSheet.PrintSetup.PaperSize = (short)PaperSize.A4;
                        break;
                    }
                case "A3":
                    {
                        WorkSheet.PrintSetup.PaperSize = (short)PaperSize.A3;
                        break;
                    }
                case "Letter":
                    {
                        WorkSheet.PrintSetup.PaperSize = (short)PaperSize.LETTER_ROTATED_PAPERSIZE;
                        break;
                    }
                default:
                    {
                        WorkSheet.PrintSetup.PaperSize = (short)PaperSize.A4;
                        break;
                    }
            }

            if (_PageSetup.SetZoom) WorkSheet.SetZoom(_PageSetup.ZoomPercent, 100);

            if (_PageSetup.FitToPage)
            {
                WorkSheet.FitToPage = true;
                WorkSheet.PrintSetup.FitHeight = (short)_PageSetup.FitHeight;
                WorkSheet.PrintSetup.FitWidth = (short)_PageSetup.FitWidth;
            }
            else
            {
                WorkSheet.FitToPage = false;
                if (_PageSetup.FitHeight > 0) { WorkSheet.PrintSetup.FitHeight = (short)_PageSetup.FitHeight; }
                if (_PageSetup.FitWidth > 0) { WorkSheet.PrintSetup.FitWidth = (short)_PageSetup.FitWidth; }
            }

        }

        public string fnGetTempPath(string _Folder)
        {
            try
            {
                String TempPath = "";
                TempPath = Environment.GetEnvironmentVariable("Temp") + "\\" + _Folder;
                if (!System.IO.Directory.Exists(TempPath)) { System.IO.Directory.CreateDirectory(TempPath); }

                return TempPath;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string fnGetBasePath(string _Folder)
        {
            try
            {
                String BasePath = "";
                BasePath = AppDomain.CurrentDomain.BaseDirectory + "\\ReportFile" + "\\" + _Folder;
                if (!System.IO.Directory.Exists(BasePath)) { System.IO.Directory.CreateDirectory(BasePath); }

                return BasePath;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ICellStyle ExcelNPOICellStyle_Title(HSSFWorkbook _wb, short _FontSize, string _FontName)
        {
            ICellStyle cStyle = _wb.CreateCellStyle();

            NPOI.SS.UserModel.IFont cFont = _wb.CreateFont();
            if (_FontName.Length > 0) { cFont.FontName = _FontName; }
            cFont.FontHeight = _FontSize;
            cFont.Boldweight = (short)FontBoldWeight.Bold;
            cStyle.SetFont(cFont);
            cStyle.Alignment = HorizontalAlignment.Center;
            cStyle.VerticalAlignment = VerticalAlignment.Top;
            cStyle.WrapText = false;

            return cStyle;
        }

        public ICellStyle ExcelNPOICellStyle_Option(HSSFWorkbook _wb, short _FontSize, string _FontName)
        {
            ICellStyle cStyle = _wb.CreateCellStyle();

            NPOI.SS.UserModel.IFont cFont = _wb.CreateFont();
            if (_FontName.Length > 0) { cFont.FontName = _FontName; }
            cFont.FontHeight = _FontSize;
            cFont.Boldweight = (short)FontBoldWeight.Bold;
            cStyle.SetFont(cFont);
            cStyle.Alignment = HorizontalAlignment.Left;
            cStyle.VerticalAlignment = VerticalAlignment.Center;
            cStyle.WrapText = true;

            return cStyle;
        }

        public ICellStyle ExcelNPOICellStyle_Header(HSSFWorkbook _wb, short _FontSize, string _FontName, Boolean _Border)
        {
            ICellStyle cStyle = _wb.CreateCellStyle();

            NPOI.SS.UserModel.IFont cFont = _wb.CreateFont();
            if (_FontName.Length > 0) { cFont.FontName = _FontName; }
            cFont.FontHeight = _FontSize;
            cFont.Boldweight = (short)FontBoldWeight.Bold;
            cStyle.SetFont(cFont);
            cStyle.Alignment = HorizontalAlignment.Center;
            cStyle.VerticalAlignment = VerticalAlignment.Center;
            cStyle.WrapText = true;

            if (_Border == true)
            {
                cStyle.BorderLeft = BorderStyle.Thin;
                cStyle.BorderRight = BorderStyle.Thin;
                cStyle.BorderTop = BorderStyle.Thin;
                cStyle.BorderBottom = BorderStyle.Thin;
            }

            return cStyle;
        }

        public ICellStyle ExcelNPOICellStyle_Cell(HSSFWorkbook _wb, short _FontSize, string _FontName, Boolean _Border, int _HAlign, int _VAlign)
        {
            ICellStyle cStyle = _wb.CreateCellStyle();
            NPOI.SS.UserModel.IFont cFont = _wb.CreateFont();
            if (_FontName.Length > 0) { cFont.FontName = _FontName; }
            cFont.FontHeight = _FontSize;
            cFont.Boldweight = (short)FontBoldWeight.Normal;
            cStyle.SetFont(cFont);

            switch (_HAlign)
            {
                case 1: { cStyle.Alignment = HorizontalAlignment.Left; break; }
                case 2: { cStyle.Alignment = HorizontalAlignment.Right; break; }
                case 3: { cStyle.Alignment = HorizontalAlignment.Center; break; }
                default: { cStyle.Alignment = HorizontalAlignment.General; break; }
            }
            switch (_VAlign)
            {
                case 1: { cStyle.VerticalAlignment = VerticalAlignment.Top; break; }
                case 2: { cStyle.VerticalAlignment = VerticalAlignment.Bottom; break; }
                case 3: { cStyle.VerticalAlignment = VerticalAlignment.Center; break; }
                default: { cStyle.VerticalAlignment = VerticalAlignment.Center; break; }
            }
            cStyle.WrapText = true;

            if (_Border == true)
            {
                cStyle.BorderLeft = BorderStyle.Thin;
                cStyle.BorderRight = BorderStyle.Thin;
                cStyle.BorderTop = BorderStyle.Thin;
                cStyle.BorderBottom = BorderStyle.Thin;
            }

            return cStyle;
        }

        public void ExcelNPOIMergeRegion(HSSFWorkbook _wb, ISheet _ws, int _iStartRow, int _iEndRow, int _iStartCol, int _iEndCol)
        {
            var CellRange = new CellRangeAddress(_iStartRow, _iEndRow, _iStartCol, _iEndCol);
            _ws.AddMergedRegion(CellRange);
        }

        public void ExcelNPOISetRegionBorder(HSSFWorkbook _wb, ISheet _ws, int _iStartRow, int _iEndRow, int _iStartCol, int _iEndCol)
        {
            var CellRange = new CellRangeAddress(_iStartRow, _iEndRow, _iStartCol, _iEndCol);

            HSSFRegionUtil.SetBorderLeft(BorderStyle.Thin, CellRange, (HSSFSheet)_ws, _wb);
            HSSFRegionUtil.SetBorderRight(BorderStyle.Thin, CellRange, (HSSFSheet)_ws, _wb);
            HSSFRegionUtil.SetBorderTop(BorderStyle.Thin, CellRange, (HSSFSheet)_ws, _wb);
            HSSFRegionUtil.SetBorderBottom(BorderStyle.Thin, CellRange, (HSSFSheet)_ws, _wb);
        }

        public void ExcelNPOICommonAssignCellValue(HSSFWorkbook _wb, ISheet _ws, int _StartRow, int _StartCol, ICellStyle _cs, DataTable _dt)
        {
            IRow r = _ws.GetRow(_StartRow);
            ICell c = r.GetCell(_StartCol);

            int Line = _StartRow, Col = _StartCol;

            foreach (DataRow row in _dt.Rows)
            {
                r = _ws.GetRow(Line);
                for (int x = 0; x < _dt.Columns.Count; x++)
                {
                    c = r.GetCell(Col);
                    c.SetCellValue(row[x].ToString());

                    c.CellStyle = _cs;
                    Col = Col + 1;
                }
                Line = Line + 1;
                Col = _StartCol;
            }
        }
    }
}