﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eHRISApi.Models;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using eHRISApi.Abstract;


namespace eHRISApi.Controllers {
    public class HomeController : Controller {
        
        public ActionResult Index() {
            //AuthorizeToken Token = new AuthorizeToken();
            //Token.token = token;
            //Token.userName = userName;
            //Token.refreshToken = refreshToken;
            //Token.useRefreshTokens = useRefreshTokens;
            //Token.directURL = directURL;
            //Token.isAuth = isAuth;

            //var serializedToken = JavaScriptConvert.SerializeObject(Token);

            //return View(serializedToken);
            return View();
        }

        public ActionResult Index2()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        [HttpGet]
        public ActionResult Login(string accessTo) {
            ViewBag.Title = "Login Page";
            //if (String.IsNullOrEmpty(accessTo)) accessTo = "listIBudgetIssueDetail"; //set the default page after login
            var serializedData = JavaScriptConvert.SerializeObject(accessTo);
            return View(serializedData);
        }

        [HttpPost]
        public ActionResult AccessPermit(string token, string userName, string refreshToken, bool useRefreshTokens, string directURL, bool isAuth) {
            AuthorizeToken Token = new AuthorizeToken();
            Token.token = token;
            Token.userName = userName;
            Token.refreshToken = refreshToken;
            Token.useRefreshTokens = useRefreshTokens;
            Token.directURL = directURL;
            Token.isAuth = isAuth;

            var serializedToken = JavaScriptConvert.SerializeObject(Token);

            return View(serializedToken);
            //return View(directURL, Token);
        }

        
    }

}
