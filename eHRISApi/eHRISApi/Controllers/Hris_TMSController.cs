﻿using eHRISApi.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace eHRISApi.Controllers
{
    public class Hris_TMSController : ApiController
    {
        private DBContexts.Contexts.HrisContext dbHris = new DBContexts.Contexts.HrisContext();

        [HttpPost]
        [Route("api/Hris_TMS/PostPromoterExportSalary")]
        [ResponseType(typeof(TMS_csp_GetPromoterExportSalary))]
        public IHttpActionResult PostPromoterExportSalary([FromBody] ParamsEntity paramsEntity)
        {
            try
            {
                var sql = @"TMS.csp_GetTmsPromoterExportSalary @Region_ID, @Company_ID, @Branch_ID, @Employee_ID, @Effective_From, @Effective_To";
                List<SqlParameter> parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter("Region_ID", paramsEntity.RegionID));
                parameterList.Add(new SqlParameter("Company_ID", paramsEntity.CompanyID));
                parameterList.Add(new SqlParameter("Branch_ID", paramsEntity.BranchID));
                parameterList.Add(new SqlParameter("Employee_ID", paramsEntity.EmployeeID));
                parameterList.Add(new SqlParameter("Effective_From", paramsEntity.EffectiveFrom.Date));
                parameterList.Add(new SqlParameter("Effective_To", paramsEntity.EffectiveTo.Date));
                SqlParameter[] parameters = parameterList.ToArray();

                List<TMS_csp_GetPromoterExportSalary> lstPromoterSalary;

                lstPromoterSalary = dbHris.Database.SqlQuery<TMS_csp_GetPromoterExportSalary>(sql, parameters).ToList();

                return Ok(lstPromoterSalary);
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("", ex);
                return Json(new { Success = false, Message = "Failed to GetPromoterExportSalary: " + ex.Message });
            }
        }
    }
}
