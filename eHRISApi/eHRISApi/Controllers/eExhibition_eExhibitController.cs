﻿using eHRISApi.Entities;
using eHRISApi.Helpers;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Description;

namespace eHRISApi.Controllers
{
    public class eExhibition_eExhibitController : ApiController
    {
        private DBContexts.Contexts.eExhibitionContext dbeExhibition = new DBContexts.Contexts.eExhibitionContext("SG");
        private ExcelNPOIHelper hpNPOI = new ExcelNPOIHelper();
        private ICellStyle cTitleStyle, cTitleStyle1, cTitleStyle2, cOptionStyle, cHeaderStyle, cCellStyle;
        private string CurrSign;

        [HttpGet]
        [Route("api/eExhibition_eExhibit/GetAESMarcomReportService")]
       // [ResponseType(typeof(string))]
        public HttpResponseMessage GetAESMarcomReportService(string _CountryCode, int _ReportCategory, int _ReportType, DateTime _StartDate, DateTime _EndDate, string _PrepareBy)
        {
            string ReportPath = "";
            dbeExhibition = new DBContexts.Contexts.eExhibitionContext(_CountryCode);

            try
            {
                switch (_ReportCategory)
                {
                    case 1:
                        ReportPath = GeneratePromoterComm(_ReportType, _StartDate, _PrepareBy, CurrSign);
                        break;
                    default:
                        throw new ArgumentException("Invalid Report Category!");
                }
            }

            catch (DivideByZeroException ex)
            {
                Console.Write(ex.Message + "\r\n" + "Cannot divide by zero. Please try again.");
            }
            catch (InvalidOperationException ex)
            {
                Console.Write(ex.Message);
            }
            catch (FormatException ex)
            {
                Console.Write(ex.Message + "\r\n" + "Invalid format. Please try again.");
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }

            Byte[] bytes = null;
            bytes = File.ReadAllBytes(ReportPath);

            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new ByteArrayContent(bytes);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

            File.Delete(ReportPath);

            return result;
        }

        private string GeneratePromoterComm(int _ReporType, DateTime _StartDate, string _PrepareBy, string _CurrSign)
        {
            string ReportPath = "";

            try
            {
                CurrSign = _CurrSign;

                switch (_ReporType)
                {
                    case (int)ConstHelpers.MarcomReportType.PromoterComm:
                        {
                            DateTime CommDate = _StartDate;
                            CommDate = new DateTime(CommDate.Year, CommDate.Month, 1).AddMonths(1).AddDays(-1);
                            ReportPath = GenPromoterComm(_ReporType, CommDate, _PrepareBy);
                            break;
                        }
                    case 2:
                        break;
                    default:
                        throw new Exception("Invalid PromoterComm Report Type!");
                }

                return ReportPath;
            }
            catch (InvalidOperationException ex)
            {
                Helper.RecordToLog("GeneratePromoterComm", ex);
                throw ex;
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("GeneratePromoterComm", ex);
                throw ex;
            }
        }

        private string GenPromoterComm(int _ReporType, DateTime _CommDate, string _PrepareBy)
        {
            try
            {
                eExhibition_csp_ReadPromoterMonthlyComm_Merged ds = GetPromoterCommReportData(_ReporType, _CommDate);
                DataTable dtDetails = new DataTable();
                DataTable dtDetails2 = new DataTable();
                DataTable dtPromoter, dtComm, dtTurnUp, dtOTS;

                dtDetails = ConvertToDataTable(ds.Promoter_Comm);
                dtDetails2 = ConvertToDataTable(ds.Promoter_CommDetails);

                if (dtDetails == null || dtDetails.Rows.Count <= 0) { throw new InvalidOperationException("No record found!"); }

                dtPromoter = dtDetails.DefaultView.ToTable(true, "Employee_ID", "Promoter_ID", "Staff_Name", "Staff_Display");

                string FileName = "\\PromoterComm_" + _CommDate.ToString("MMMyy") + "_" + DateTime.Now.ToString("ddMMyyHHmmss") + ".xls";
                FileName = hpNPOI.fnGetBasePath("PromoterComm") + FileName;
                HSSFWorkbook NPOIWBook;
                NPOIWBook = new HSSFWorkbook();
                NPOIWBook = hpNPOI.ExcelNPOICreate("PromoterComm");

                ISheet NPOIWSheet = NPOIWBook.GetSheetAt(0);
                {
                    var ws = NPOIWSheet;
                    var wb = NPOIWBook;
                    ws.DefaultColumnWidth = 9;
                    ws.DefaultRowHeight = 13;
                    ws.DisplayGridlines = false;
                    ws.PrintSetup.PaperSize = 9;
                    ws.PrintSetup.Landscape = true;
                    ws.PrintSetup.FitWidth = 1;
                    ws.PrintSetup.FitHeight = 1;
                    ws.SetMargin(NPOI.SS.UserModel.MarginType.HeaderMargin, Convert.ToDouble(0.3));
                    ws.SetMargin(NPOI.SS.UserModel.MarginType.FooterMargin, Convert.ToDouble(0.3));
                    ws.SetMargin(NPOI.SS.UserModel.MarginType.TopMargin, Convert.ToDouble(0.4));
                    ws.SetMargin(NPOI.SS.UserModel.MarginType.BottomMargin, Convert.ToDouble(0.4));
                    ws.SetMargin(NPOI.SS.UserModel.MarginType.LeftMargin, Convert.ToDouble(0.4));
                    ws.SetMargin(NPOI.SS.UserModel.MarginType.RightMargin, Convert.ToDouble(0.4));
                    ws.Header.Right = "&\"Arial Narrow\"&8 Page &P of &N";

                    // short pg = ws.PrintSetup.PaperSize;
                    cTitleStyle = hpNPOI.ExcelNPOICellStyle_Title(wb, 240, "Arial");
                    cTitleStyle1 = hpNPOI.ExcelNPOICellStyle_Title(wb, 200, "Arial");
                    cOptionStyle = hpNPOI.ExcelNPOICellStyle_Option(wb, 160, "Arial Narrow");
                    cTitleStyle2 = hpNPOI.ExcelNPOICellStyle_Title(wb, 200, "Arial Narrow");
                    cHeaderStyle = hpNPOI.ExcelNPOICellStyle_Header(wb, 140, "Arial Narrow", true);
                    cCellStyle = hpNPOI.ExcelNPOICellStyle_Cell(wb, 160, "Arial Narrow", true, 3, 3);

                    int StartLine = -42, Line = 0, LineSumm = 30, LineSign = 38;
                    int ColCM = 0, ColTU = 9, ColOT = 16;

                    //IRow r;
                    for (int P = 0; P < dtPromoter.Rows.Count; P++)
                    {
                        StartLine = StartLine + 42;
                        Line = StartLine;

                        dtDetails.DefaultView.RowFilter = "Comm_Type='COMM' and Employee_ID='" + dtPromoter.Rows[P]["Employee_ID"] + "' and Promoter_ID='" + dtPromoter.Rows[P]["Promoter_ID"] + "'";
                        dtComm = dtDetails.DefaultView.ToTable(true, "Exhibition_ID", "Comm_Scheme", "Claim_Date", "Total_Voucher", "Total_Claim", "Rate", "Claim_Amount");
                        dtDetails.DefaultView.RowFilter = "Comm_Type='TURNUP' and Employee_ID='" + dtPromoter.Rows[P]["Employee_ID"] + "' and Promoter_ID='" + dtPromoter.Rows[P]["Promoter_ID"] + "'";
                        dtTurnUp = dtDetails.DefaultView.ToTable();
                        dtDetails.DefaultView.RowFilter = "Comm_Type='OTS' and Employee_ID='" + dtPromoter.Rows[P]["Employee_ID"] + "' and Promoter_ID='" + dtPromoter.Rows[P]["Promoter_ID"] + "'";
                        dtOTS = dtDetails.DefaultView.ToTable(true, "Claim_Date", "Exhibition_ID", "Total_Claim", "Claim_Amount");

                        var hsReport = new Hashtable();
                        hsReport.Add("Line", Line);
                        hsReport.Add("ColCM", ColCM);
                        hsReport.Add("ColTU", ColTU);
                        hsReport.Add("ColOT", ColOT);
                        hsReport.Add("EmployeeID", dtPromoter.Rows[P]["Employee_ID"]);
                        hsReport.Add("StaffDisplay", dtPromoter.Rows[P]["Staff_Display"]);
                        GenPromoterComm_Header(wb, ws, hsReport, _CommDate);

                        //if (dtComm.Rows.Count > 0) { }
                        GenPromoterComm_Comm(wb, ws, dtComm, Line + 5, ColCM);
                        GenPromoterComm_TurnUp(wb, ws, dtTurnUp, Line + 5, ColTU);
                        GenPromoterComm_OTS(wb, ws, dtOTS, Line + 5, ColOT);

                        hsReport = new Hashtable();
                        hsReport.Add("Line", Line + LineSumm);

                        //dtComm.Compute("sum(claim_amount)", "claim_amount<>0")(0);
                        hsReport.Add("Commission", dtComm.AsEnumerable().Sum(r => r.Field<decimal>("claim_amount")));
                        hsReport.Add("TurnUp", dtTurnUp.AsEnumerable().Sum(r => r.Field<decimal>("claim_amount")));
                        hsReport.Add("OTS", dtOTS.AsEnumerable().Sum(r => r.Field<decimal>("claim_amount")));
                        GenPromoterComm_Summ(wb, ws, hsReport);

                        hsReport = new Hashtable();
                        hsReport.Add("Line", Line + LineSign);// PrepareBy
                        hsReport.Add("PrepareBy", _PrepareBy);
                        GenPromoterComm_Sign(wb, ws, hsReport);
                    }

                    ws = NPOIWBook.GetSheetAt(0);
                    ws.SetColumnBreak(20);
                    for (int rowCount = 41; rowCount < ws.LastRowNum; rowCount += 42)
                    {
                        ws.SetRowBreak(rowCount);
                    }
                    FileStream fs = new FileStream(FileName, FileMode.Create, FileAccess.ReadWrite);
                    wb.Write(fs);
                    fs.Close();

                    //Microsoft.Office.Interop.Excel.Application xlsApp;
                    //Microsoft.Office.Interop.Excel._Workbook xlsWB;
                    //Microsoft.Office.Interop.Excel.Worksheet xlsWS;
                    //xlsApp = new Microsoft.Office.Interop.Excel.Application();

                    //xlsWB = xlsApp.Workbooks.Open(FileName);
                    //xlsWS = (Microsoft.Office.Interop.Excel.Worksheet)xlsWB.Sheets["PromoterComm"];
                    //var xlsPG = xlsWS.PageSetup;
                    //xlsPG.PaperSize = Microsoft.Office.Interop.Excel.XlPaperSize.xlPaperA4;
                    //xlsPG.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;
                    //xlsPG.FitToPagesWide = 1;
                    //xlsPG.FooterMargin = xlsWS.Application.CentimetersToPoints(0.3);
                    //xlsPG.HeaderMargin = xlsWS.Application.CentimetersToPoints(0.3);
                    //xlsPG.RightHeader = "&\"Arial Narrow\"&8 Page &P of &N";
                    //xlsPG.TopMargin = xlsWS.Application.CentimetersToPoints(1);
                    //xlsPG.BottomMargin = xlsWS.Application.CentimetersToPoints(0.8);
                    //xlsPG.LeftMargin = xlsWS.Application.CentimetersToPoints(1);
                    //xlsPG.RightMargin = xlsWS.Application.CentimetersToPoints(0.8);
                    //xlsWB.Save();
                    //xlsWB.Close();
                    //xlsApp.Quit();
                    //System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsApp);
                    //xlsApp = null;

                    //NPOIS printsetup not working well
                    //IPrintSetup print = wb.GetSheetAt(0).PrintSetup;
                    //print.Landscape = true;                   
                    //print.FitWidth = (short)1;
                    //print.FitHeight = (short)999;
                    //print.PaperSize= (short)PaperSize.A4;
                    //print.HeaderMargin = 0.2;
                    //print.FooterMargin = 0.2;
                    //print.NoOrientation = false;
                    //ws.FitToPage = false;
                    //ws.PrintSetup.PaperSize = (short)PaperSize.A4;
                    //ws.PrintSetup.FitWidth = 1;
                    //ws.PrintSetup.FitHeight = 999;     
                    //ws.SetMargin(MarginType.TopMargin, 0.4);
                    //ws.SetMargin(MarginType.BottomMargin, 0.4);
                    //ws.SetMargin(MarginType.LeftMargin, 0.4);
                    //ws.SetMargin(MarginType.RightMargin, 0.4);
                    //ws.PrintSetup.Landscape = true;

                }

                //System.Diagnostics.Process.Start("explorer.exe", FileName);
                return FileName;
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("GenPromoterComm", ex);
                throw ex;
            }
        }

        private void GenPromoterComm_Header(HSSFWorkbook _wb, ISheet _ws, Hashtable _hsReport, DateTime _CommDate)
        {
            ICell cell;
            IRow r;

            int Line = (int)_hsReport["Line"];
            int ColCM = (int)_hsReport["ColCM"], ColTU = (int)_hsReport["ColTU"], ColOT = (int)_hsReport["ColOT"];
            try
            {
                for (int l = Line; l < Line + 42; l++)
                {
                    r = (_ws.GetRow(l) == null) ? _ws.CreateRow(l) : _ws.GetRow(l);
                    for (int c = 0; c < 21; c++)
                    {
                        cell = r.CreateCell(c);
                        if (Line == 0)
                        {
                            switch (c)
                            {
                                case 3:
                                case 6:
                                case 16:
                                    {
                                        _ws.SetColumnWidth(c, Convert.ToInt32(Math.Round(6.5 * 256)));
                                        break;
                                    }
                                case 4:
                                case 5:
                                case 10:
                                case 11:
                                case 12:
                                case 13:
                                case 18:
                                    {
                                        _ws.SetColumnWidth(c, Convert.ToInt32(Math.Round(5.5 * 256)));
                                        break;
                                    }
                                case 8:
                                case 15:
                                    {
                                        _ws.SetColumnWidth(c, Convert.ToInt32(Math.Round(1.5 * 256)));
                                        break;
                                    }
                                default:
                                    {
                                        _ws.SetColumnWidth(c, Convert.ToInt32(Math.Round(9.5 * 256)));
                                        break;
                                    }
                            }
                        }
                    }
                }

                r = _ws.GetRow(Line);
                r.HeightInPoints = 15;
                hpNPOI.ExcelNPOIMergeRegion(_wb, _ws, Line, Line + 1, 0, 3);
                r.GetCell(0).SetCellValue("Employee ID : " + _hsReport["EmployeeID"] + "\r\n" + "Name : " + _hsReport["StaffDisplay"]);
                r.GetCell(0).CellStyle = cOptionStyle;
                hpNPOI.ExcelNPOIMergeRegion(_wb, _ws, Line, Line, 4, 14);
                r.GetCell(4).SetCellValue("Exhibition Promoter Commission & Incentive");
                r.GetCell(4).CellStyle = cTitleStyle;
                hpNPOI.ExcelNPOIMergeRegion(_wb, _ws, Line + 1, Line + 1, 4, 14);
                r = _ws.GetRow(Line + 1);
                r.HeightInPoints = 18;
                r.GetCell(4).SetCellValue("Report Date : " + _CommDate.ToString("MMM yyyy"));
                r.GetCell(4).CellStyle = cTitleStyle1;

                Line = Line + 2;
                r = _ws.GetRow(Line);
                cell = r.GetCell(ColCM);
                cell.SetCellValue("Commission");
                cell.CellStyle = cTitleStyle2;
                cell = r.GetCell(ColTU);
                cell.SetCellValue("Turn Up");
                cell.CellStyle = cTitleStyle2;
                cell = r.GetCell(ColOT);
                cell.SetCellValue("OTS");
                cell.CellStyle = cTitleStyle2;

                Line = Line + 1;
                r = _ws.GetRow(Line);
                ICell cell2 = r.GetCell(0);

                for (int c = 0; c <= 19; c++)
                {
                    if (c != 8 && c != 15)
                    {
                        cell = r.GetCell(c);
                        cell2 = _ws.GetRow(Line + 1).GetCell(c);

                        switch (c)
                        {
                            case 0: cell.SetCellValue("Exhibition"); break;
                            case 1:
                                {
                                    cell.SetCellValue("Scheme");
                                    hpNPOI.ExcelNPOIMergeRegion(_wb, _ws, Line, Line + 1, c, c + 1);
                                    break;
                                }
                            case 3: cell.SetCellValue("Date"); break;
                            case 4:
                                {
                                    cell.SetCellValue("Voucher Sold");
                                    hpNPOI.ExcelNPOIMergeRegion(_wb, _ws, Line, Line, c, c + 1);
                                    cell2.SetCellValue("Total");
                                    break;
                                }
                            case 5: cell2.SetCellValue("Claim"); break;
                            case 6:
                                {
                                    cell.SetCellValue("Commission");
                                    hpNPOI.ExcelNPOIMergeRegion(_wb, _ws, Line, Line, c, c + 1);
                                    cell2.SetCellValue("Rate");
                                    break;
                                }
                            case 7: cell2.SetCellValue("Claim"); break;
                            case 9: cell.SetCellValue("Brand"); break;
                            case 10:
                                {
                                    cell.SetCellValue("Main");
                                    hpNPOI.ExcelNPOIMergeRegion(_wb, _ws, Line, Line, c, c + 1);
                                    cell2.SetCellValue("Rate");
                                    break;
                                }
                            case 11: cell2.SetCellValue("Visit"); break;
                            case 12:
                                {
                                    cell.SetCellValue("Others");
                                    hpNPOI.ExcelNPOIMergeRegion(_wb, _ws, Line, Line, c, c + 1);
                                    cell2.SetCellValue("Rate");
                                    break;
                                }
                            case 13: cell2.SetCellValue("Visit"); break;
                            case 14: cell.SetCellValue("Sub Total"); break;
                            case 16: cell.SetCellValue("Date"); break;
                            case 17: cell.SetCellValue("Exhibition"); break;
                            case 18: cell.SetCellValue("OTS"); break;
                            case 19: cell.SetCellValue("Sub Total"); break;
                        }
                        cell.CellStyle = cHeaderStyle;
                        cell2.CellStyle = cHeaderStyle;
                        if (c >= 0 && c <= 3) { hpNPOI.ExcelNPOIMergeRegion(_wb, _ws, Line, Line + 1, c, c); }
                        else if (c == 9) { hpNPOI.ExcelNPOIMergeRegion(_wb, _ws, Line, Line + 1, c, c); }
                        else if (c >= 14) { hpNPOI.ExcelNPOIMergeRegion(_wb, _ws, Line, Line + 1, c, c); }
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("GenPromoterComm_Header", ex);
                throw ex;
            }

        }

        private void GenPromoterComm_Comm(HSSFWorkbook _wb, ISheet _ws, System.Data.DataTable _dtComm, int _StartLine, int _StartCol)
        {
            int Line = _StartLine, Col = _StartCol;
            IRow r = _ws.GetRow(Line);
            ICell c = r.GetCell(Col);
            IDataFormat dataFormatCustom = _wb.CreateDataFormat();
            ICellStyle cs = hpNPOI.ExcelNPOICellStyle_Header(_wb, 180, "Arial Narrow", true);

            cCellStyle.DataFormat = dataFormatCustom.GetFormat("#,##0");
            cs.DataFormat = dataFormatCustom.GetFormat("#,##0");
            try
            {
                foreach (DataRow row in _dtComm.Rows)
                {
                    r = _ws.GetRow(Line);
                    for (int x = 0; x < 8; x++)
                    {
                        c = r.GetCell(Col);
                        DateTime dtClaimDate = (DateTime)row["Claim_Date"];
                        switch (x)
                        {
                            case 0: { c.SetCellValue(row["Exhibition_ID"].ToString()); break; }
                            case 1: { c.SetCellValue(row["Comm_Scheme"].ToString()); break; }
                            case 2: { hpNPOI.ExcelNPOIMergeRegion(_wb, _ws, Line, Line, 1, 2); break; }
                            case 3: { c.SetCellValue(dtClaimDate.ToString("dd-MMM")); break; }
                            case 4: { c.SetCellValue((int)row["Total_Voucher"]); break; }
                            case 5: { c.SetCellValue((int)row["Total_Claim"]); break; }
                            case 6: { c.SetCellValue(Convert.ToDouble(row["Rate"])); break; }
                            case 7: { c.SetCellValue(Convert.ToDouble(row["Claim_Amount"])); break; }
                        }
                        if (c != null) { c.CellStyle = cCellStyle; }
                        Col = Col + 1;
                    }
                    Line = Line + 1;
                    Col = _StartCol;
                }
                double TotalAmt = 0;
                TotalAmt = _dtComm.Rows.Count > 0 ? Convert.ToDouble(_dtComm.Compute("Sum(Claim_Amount)", "")) : 0.0;

                r = _ws.GetRow(Line);
                for (int x = 0; x < 8; x++)
                {
                    c = r.GetCell(Col);
                    c.CellStyle = cs;
                    switch (x)
                    {
                        case 0: { c.SetCellValue("Total"); hpNPOI.ExcelNPOIMergeRegion(_wb, _ws, Line, Line, Col, Col + 3); break; }
                        case 4: { c.SetCellValue(_dtComm.AsEnumerable().Sum(dtR => dtR.Field<int>("Total_Voucher"))); break; }
                        case 5: { c.SetCellValue(_dtComm.AsEnumerable().Sum(dtR => dtR.Field<int>("Total_Claim"))); break; }
                        case 7: { c.SetCellValue(CurrSign + " " + TotalAmt.ToString("#,##0")); break; }
                    }
                    Col = Col + 1;
                }
            }
            catch (Exception ex)
            { Helper.RecordToLog("GenPromoterComm_Comm", ex); throw ex; }

        }

        private void GenPromoterComm_TurnUp(HSSFWorkbook _wb, ISheet _ws, System.Data.DataTable _dtTurnUp, int _StartLine, int _StartCol)
        {
            int Line = _StartLine, Col = _StartCol;
            IRow r = _ws.GetRow(Line);
            ICell c = r.GetCell(Col);
            IDataFormat dataFormatCustom = _wb.CreateDataFormat();
            ICellStyle cs = hpNPOI.ExcelNPOICellStyle_Header(_wb, 180, "Arial Narrow", true);
            cCellStyle.DataFormat = dataFormatCustom.GetFormat("#,##0");
            cs.DataFormat = dataFormatCustom.GetFormat("#,##0");

            DataTable dtCo = _dtTurnUp.DefaultView.ToTable(true, "Company_ID", "Company_Code", "Company_Sort");
            DataTable dtTUMain, dtTUOther;

            try
            {
                foreach (DataRow row in dtCo.Rows)
                {
                    r = _ws.GetRow(Line);
                    dtTUMain = _dtTurnUp.Copy();
                    dtTUMain.DefaultView.RowFilter = string.Format("Company_ID='{0}' and Comm_Scheme='MAIN'", row["Company_id"]);
                    dtTUMain = dtTUMain.DefaultView.ToTable();

                    dtTUOther = _dtTurnUp.Copy();
                    dtTUOther.DefaultView.RowFilter = string.Format("Company_ID='{0}' and Comm_Scheme='OTHERS'", row["Company_id"]);
                    dtTUOther = dtTUOther.DefaultView.ToTable();
                    //.Where(r => decimal.TryParse(r.Field<string>("Col1"), out d)) .Sum(r => d);


                    double ClaimAmt = Convert.ToDouble(dtTUMain.Compute("Sum(Claim_Amount)", "")) + Convert.ToDouble(dtTUOther.Compute("Sum(Claim_Amount)", ""));
                    for (int x = 0; x < 6; x++)
                    {
                        c = r.GetCell(Col);

                        switch (x)
                        {
                            case 0: { c.SetCellValue(row["Company_Code"].ToString()); break; }
                            case 1: { c.SetCellValue(Convert.ToDouble(dtTUMain.Compute("Max(Rate)", ""))); break; }
                            case 2: { c.SetCellValue(Convert.ToInt32(dtTUMain.Compute("Sum(Total_Claim)", ""))); break; }
                            case 3: { c.SetCellValue(Convert.ToDouble(dtTUOther.Compute("Max(Rate)", ""))); break; }
                            case 4: { c.SetCellValue(Convert.ToInt32(dtTUOther.Compute("Sum(Total_Claim)", ""))); break; }
                            case 5: { c.SetCellValue(ClaimAmt); break; }
                        }
                        if (c != null) { c.CellStyle = cCellStyle; }
                        Col = Col + 1;
                    }
                    Line = Line + 1;
                    Col = _StartCol;
                }

                double TotalAmt = _dtTurnUp.Rows.Count > 0 ? Convert.ToDouble(_dtTurnUp.Compute("Sum(Claim_Amount)", "")) : 0.0; //Convert.ToDouble(_dtTurnUp.Compute("Sum(Claim_Amount)", ""));

                dtTUMain = _dtTurnUp.Copy();
                dtTUMain.DefaultView.RowFilter = "Comm_Scheme='MAIN'";
                dtTUMain = dtTUMain.DefaultView.ToTable();

                dtTUOther = _dtTurnUp.Copy();
                dtTUOther.DefaultView.RowFilter = "Comm_Scheme='OTHERS'";
                dtTUOther = dtTUOther.DefaultView.ToTable();

                r = _ws.GetRow(Line);
                for (int x = 0; x < 6; x++)
                {
                    c = r.GetCell(Col);
                    c.CellStyle = cs;
                    switch (x)
                    {
                        case 0: { c.SetCellValue("Total"); break; }
                        case 2: { c.SetCellValue(dtTUMain.Rows.Count > 0 ? Convert.ToDouble(dtTUMain.Compute("Sum(Total_Claim)", "")) : 0.0); break; }
                        case 4: { c.SetCellValue(dtTUOther.Rows.Count > 0 ? Convert.ToDouble(dtTUOther.Compute("Sum(Total_Claim)", "")) : 0.0); break; }
                        case 5: { c.SetCellValue(CurrSign + " " + TotalAmt.ToString("#,##0")); break; }
                    }
                    Col = Col + 1;
                }
            }
            catch (Exception ex)
            { Helper.RecordToLog("GenPromoterComm_TurnUp", ex); throw ex; }

        }

        private void GenPromoterComm_OTS(HSSFWorkbook _wb, ISheet _ws, System.Data.DataTable _dtOTS, int _StartLine, int _StartCol)
        {
            IRow r = _ws.GetRow(_StartLine);
            ICell c = r.GetCell(_StartCol);
            IDataFormat dataFormatCustom = _wb.CreateDataFormat();
            ICellStyle cs = cCellStyle;
            ICellStyle cs2 = hpNPOI.ExcelNPOICellStyle_Header(_wb, 180, "Arial Narrow", true);
            cs.DataFormat = dataFormatCustom.GetFormat("#,##0");

            int Line = _StartLine, Col = _StartCol;

            try
            {
                foreach (DataRow row in _dtOTS.Rows)
                {
                    r = _ws.GetRow(Line);
                    for (int x = 0; x < 4; x++)
                    {
                        c = r.GetCell(Col);
                        DateTime dtClaimDate = (DateTime)row["Claim_Date"];
                        switch (x)
                        {
                            case 0: { c.SetCellValue(dtClaimDate.ToString("dd-MMM")); break; }
                            case 1: { c.SetCellValue(row["Exhibition_ID"].ToString()); break; }
                            case 2: { c.SetCellValue((int)row["Total_Claim"]); break; }
                            case 3: { c.SetCellValue(Convert.ToDouble(row["Claim_Amount"])); break; }
                        }
                        if (c != null) { c.CellStyle = cs; }
                        Col = Col + 1;
                    }
                    Line = Line + 1;
                    Col = _StartCol;
                }
                double TotatAmt = 0;
                TotatAmt = _dtOTS.Rows.Count > 0 ? Convert.ToDouble(_dtOTS.Compute("Sum(Claim_Amount)", "")) : 0.0;

                r = _ws.GetRow(Line);
                for (int x = 0; x < 4; x++)
                {
                    c = r.GetCell(Col);

                    switch (x)
                    {
                        case 0: { c.SetCellValue("Total"); hpNPOI.ExcelNPOIMergeRegion(_wb, _ws, Line, Line, Col, Col + 2); break; }
                        case 3: { c.SetCellValue(CurrSign + " " + TotatAmt.ToString("#,##0")); break; }
                    }
                    c.CellStyle = cs2;
                    Col = Col + 1;
                }
            }
            catch (Exception ex)
            { Helper.RecordToLog("GenPromoterComm_OTS", ex); throw ex; }


        }

        private void GenPromoterComm_Summ(HSSFWorkbook _wb, ISheet _ws, Hashtable _hsReport)
        {


            int Line = (int)_hsReport["Line"];
            double AmtCommission = Convert.ToDouble(_hsReport["Commission"]);
            double AmtTurnUp = Convert.ToDouble(_hsReport["TurnUp"]);
            double AmtOTS = Convert.ToDouble(_hsReport["OTS"]);
            double AmtTotal = AmtCommission + AmtTurnUp + AmtOTS;
            IRow r;
            r = _ws.GetRow(Line);

            IDataFormat dataFormatCustom = _wb.CreateDataFormat();
            ICell cell = r.GetCell(0);
            ICell cell2 = r.GetCell(0);
            ICellStyle cs = hpNPOI.ExcelNPOICellStyle_Header(_wb, 180, "Arial Narrow", true);
            cs.WrapText = false;

            try
            {
                for (int c = 0; c <= 4; c++)
                {
                    cell = r.GetCell(c);
                    cell2 = _ws.GetRow(Line + 1).GetCell(c);
                    switch (c)
                    {
                        case 0: { cell.SetCellValue("Commission"); hpNPOI.ExcelNPOIMergeRegion(_wb, _ws, Line, Line + 1, c, c); break; }
                        case 1: { cell.SetCellValue("Incentive"); hpNPOI.ExcelNPOIMergeRegion(_wb, _ws, Line, Line, c, c + 1); cell2.SetCellValue("Turn Up"); break; }
                        case 2: { cell2.SetCellValue("OTS"); break; }
                        case 3: { cell.SetCellValue("Total"); hpNPOI.ExcelNPOIMergeRegion(_wb, _ws, Line, Line + 1, c, c + 1); break; }

                    }

                    cell.CellStyle = cs;
                    cell2.CellStyle = cs;
                }
                cs = hpNPOI.ExcelNPOICellStyle_Header(_wb, 200, "Arial Narrow", true);
                r = _ws.GetRow(Line + 2);
                r.GetCell(0).SetCellValue(CurrSign + " " + AmtCommission.ToString("#,##0"));
                r.GetCell(0).CellStyle = cs;
                r.GetCell(1).SetCellValue(CurrSign + " " + AmtTurnUp.ToString("#,##0"));
                r.GetCell(1).CellStyle = cs;
                r.GetCell(2).SetCellValue(CurrSign + " " + AmtOTS.ToString("#,##0"));
                r.GetCell(2).CellStyle = cs;
                r.GetCell(3).SetCellValue(CurrSign + " " + AmtTotal.ToString("#,##0"));
                r.GetCell(3).CellStyle = cs;
                r.GetCell(4).CellStyle = cs;
                hpNPOI.ExcelNPOIMergeRegion(_wb, _ws, Line + 2, Line + 2, 3, 4);
            }
            catch (Exception ex)
            { Helper.RecordToLog("GenPromoterComm_Summ", ex); throw ex; }

        }

        private void GenPromoterComm_Sign(HSSFWorkbook _wb, ISheet _ws, Hashtable _hsReport)
        {

            int Line = (int)_hsReport["Line"];
            string PrepareBy = (string)_hsReport["PrepareBy"];
            ICellStyle cLeftStyle = hpNPOI.ExcelNPOICellStyle_Cell(_wb, 140, "Arial", false, 2, 3);
            cLeftStyle.WrapText = false;
            cLeftStyle.VerticalAlignment = VerticalAlignment.Bottom;
            ICellStyle cBottomStyle = hpNPOI.ExcelNPOICellStyle_Cell(_wb, 140, "", false, 3, 2);
            cBottomStyle.BorderBottom = BorderStyle.Thin;

            ICell cell;

            string str = "";

            try
            {
                for (int c = 0; c <= 3; c++)
                {
                    int col = 0, colLen = 0;
                    switch (c)
                    {
                        case 0: { col = 0; colLen = 2; str = "Prepared"; break; }
                        case 1: { col = 4; colLen = 3; str = "Reviewed"; break; }
                        case 2: { col = 9; colLen = 4; str = "Approved"; break; }
                        case 3: { col = 15; colLen = 3; str = "Approved"; break; }
                    }
                    cell = _ws.GetRow(Line).GetCell(col);
                    cell.SetCellValue("Signature      :");
                    cell.CellStyle = cLeftStyle;

                    cell = _ws.GetRow(Line + 1).GetCell(col);
                    cell.SetCellValue(str + " By :");
                    cell.CellStyle = cLeftStyle;

                    cell = _ws.GetRow(Line + 2).GetCell(col);
                    cell.SetCellValue(str + " On :");
                    cell.CellStyle = cLeftStyle;


                    hpNPOI.ExcelNPOIMergeRegion(_wb, _ws, Line, Line, col + 1, col + colLen);
                    hpNPOI.ExcelNPOIMergeRegion(_wb, _ws, Line + 1, Line + 1, col + 1, col + colLen);
                    hpNPOI.ExcelNPOIMergeRegion(_wb, _ws, Line + 2, Line + 2, col + 1, col + colLen);

                    for (int r = 0; r < 3; r++)
                    {
                        for (int x = 0; x < colLen; x++)
                        {
                            cell = _ws.GetRow(Line + r).GetCell(col + 1 + x);
                            cell.CellStyle = cBottomStyle;
                            if (col == 0 && r == 1) { cell.SetCellValue(PrepareBy); }
                            if (col == 0 && r == 2) { cell.SetCellValue(DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt")); }
                        }
                    }

                }
            }
            catch (Exception ex)
            { Helper.RecordToLog("GenPromoterComm_Sign", ex); throw ex; }
        }

        private eExhibition_csp_ReadPromoterMonthlyComm_Merged GetPromoterCommReportData(int _ReporType, DateTime _CommDate)
        {
            var command = dbeExhibition.Database.Connection.CreateCommand();
            DbParameter param = command.CreateParameter();
            eExhibition_csp_ReadPromoterMonthlyComm_Merged promoterComm = new eExhibition_csp_ReadPromoterMonthlyComm_Merged();

            try
            {
                int ReportType = _ReporType;

                dbeExhibition.Database.CommandTimeout = 960;

                switch (ReportType)
                {

                    case (int)ConstHelpers.MarcomReportType.PromoterComm:
                    case (int)ConstHelpers.MarcomReportType.PromoterCommDetails:
                        {
                            command.CommandText = "Reports.csp_ReadPromoterMonthlyComm";
                            param = command.CreateParameter();
                            param.ParameterName = "@Comm_Date";
                            param.Value = Convert.ToDateTime(_CommDate).Date;
                            command.Parameters.Add(param);

                            param = command.CreateParameter();
                            param.ParameterName = "@Report_Type";
                            param.Value = ReportType;
                            command.Parameters.Add(param);
                            break;
                        };

                    default:
                        throw new Exception("Invalid Report Type!");
                }


                command.CommandType = CommandType.StoredProcedure;
                dbeExhibition.Database.Connection.Open();

                var reader = command.ExecuteReader();

                promoterComm.Promoter_Comm = ((IObjectContextAdapter)dbeExhibition).ObjectContext.Translate<eExhibition_csp_ReadPromoterMonthlyComm_Summary>(reader).ToList();
                reader.NextResult();
                promoterComm.Promoter_CommDetails = ((IObjectContextAdapter)dbeExhibition).ObjectContext.Translate<eExhibition_csp_ReadPromoterMonthlyComm_Details>(reader).ToList();
            }
            catch (Exception ex)
            {
                Helper.RecordToLog("GetPromoterCommReportData", ex);
                throw ex;
            }

            return promoterComm;
        }

        private DataTable ConvertToDataTable<T>(List<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }
    }
}
