﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eHRISApi.Models {
    public class AuthorizeToken {
        public string token { get; set; }

        public string userName { get; set; }

        public string refreshToken { get; set; }

        public string directURL { get; set; }

        public bool useRefreshTokens { get; set; }

        public bool isAuth { get; set; }

    }

    public class ResetPasswordViewModel {
        public string userId { get; set; }
        public string code { get; set; }
    }
}