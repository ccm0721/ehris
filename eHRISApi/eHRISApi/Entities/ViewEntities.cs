﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace eHRISApi.Entities {
    public class filterParamsViewEntity {
        public string key { get; set; }
        public string ops { get; set; }
        public string value { get; set; }
    }
    
    public class PageSettingViewEntity {
        public string referId { get; set; }
        public int pageNo { get; set; }
        public int pageSize { get; set; }
        public string sortColumnName { get; set; }
        public string sortOrder { get; set; }
        public filterParamsViewEntity[] filterParams { get; set; }
        public string parameters { get; set; }
    }

    public class ParamsEntity
    {
        public string RegionID { get; set; }
        public string CompanyID { get; set; }
        public string BranchID { get; set; }
        public string EmployeeID { get; set; }
        public DateTime EffectiveFrom { get; set; }
        public DateTime EffectiveTo { get; set; }
    }

    public class TMS_csp_GetPromoterExportSalary
    {
        public Guid NewGuid { get; set; }
        public string Region_ID { get; set; }
        public string Company_ID { get; set; }
        public string Employee_ID { get; set; }
        public string Payroll_ID { get; set; }
        //public string Salary_Rate_Type { get; set; }
        //public decimal Salary_Rate { get; set; }
        public string OT_Date { get; set; }
        public decimal Total_Basic_Salary { get; set; }
        public int Lateness_Flag { get; set; }
        public string Exhibition_ID { get; set; } 
    }

    public class eExhibition_csp_ReadPromoterMonthlyComm_Merged
    {
        public List<eExhibition_csp_ReadPromoterMonthlyComm_Summary> Promoter_Comm { get; set; }
        public List<eExhibition_csp_ReadPromoterMonthlyComm_Details> Promoter_CommDetails { get; set; }
    }

    public class eExhibition_csp_ReadPromoterMonthlyComm_Summary
    {
        public string Employee_ID { get; set; }
        public string Promoter_ID { get; set; }
        public string Company_ID { get; set; }
        public string Exhibition_ID { get; set; }
        public DateTime Claim_Date { get; set; }
        public DateTime Comm_Date { get; set; }
        public string Comm_Type { get; set; }
        public string Comm_Scheme { get; set; }
        public int Total_Voucher { get; set; }
        public int Total_Claim { get; set; }
        public decimal Rate { get; set; }
        public decimal Claim_Amount { get; set; }
        public int Comm_Sort { get; set; }
        public string Staff_Name { get; set; }
        public string Staff_Display { get; set; }
        public DateTime Join_Date { get; set; }
        public string Company_Code { get; set; }
        public int Company_Sort { get; set; }
        public string Country_Code { get; set; }
        public string Prepare_By { get; set; }
    }

    public class eExhibition_csp_ReadPromoterMonthlyComm_Details
    {
        public string Company_ID { get; set; }
        public string Company_Code { get; set; }
        public string Scheme_ID { get; set; }
        public string Scheme_Name { get; set; }
        public string Rate_Desc { get; set; }
        public string claim_method { get; set; }
        public string claim_group { get; set; }
        public int Min { get; set; }
        public int Max { get; set; }
        public int Value { get; set; }
        public string Accumulative { get; set; }
        public string Company_Sort { get; set; }
    }

}